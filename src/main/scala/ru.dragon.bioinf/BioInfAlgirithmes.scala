package ru.dragon.bioinf

import ru.dragon.helpers.EdgeColors._

import scala.annotation.tailrec
import scala.collection._
import scala.collection.immutable.TreeMap

/**
 * @author NIzhikov
 */
trait BioInfAlgorithmes extends Translation {
    implicit class BioInfString(data: String) {
        def patternIndexes(p: String): List[Int] = {
            val l = data.length
            val pl = p.length

            val toNextStep = (m: List[Int], i: Int) => m.filter(x => p(x) == data(i)).map(_+1)

            @tailrec
            def doCount(i: Int, acc: List[Int], matches: List[Int]): List[Int] = {
                if (data.length == i)
                    matches match {
                        case Nil => acc
                        case h :: tail => if (h == pl) acc :+ (i-pl) else acc
                    }
                else {
                    val nm = if (p(0) == data(i)) List(1) else Nil
                    matches match {
                        case Nil =>
                            doCount(i+1, acc, nm)
                        case h :: tail =>
                            if (h == pl)
                                doCount(i+1, acc :+ (i-pl), toNextStep(tail, i) ++ nm)
                            else
                                doCount(i+1, acc, toNextStep(matches, i) ++ nm)

                    }
                }

            }

            doCount(0, Nil, Nil)
        }

        def patternCount(p: String) = patternIndexes(p).length

        def matchCount(p: Char, i: Int): Int = {
            @tailrec
            def matchCount0(j: Int, acc: Int): Int =
                if (j == i)
                    acc
                else if (data(j) == p)
                    matchCount0(j+1, acc+1)
                else
                    matchCount0(j+1, acc)

            matchCount0(0, 0)
        }

        def skew(i: Int) = matchCount('G', i) - matchCount('C', i)

        def minSkew = {
            @tailrec
            def minSkew0(i: Int, v: Int, min: Int, acc: List[Int]): List[Int] = {
                if (i == data.length)
                    acc
                else if (data(i) == 'G')
                    minSkew0(i+1, v+1, min, acc)
                else if (data(i) == 'C') {
                    val diff = v - min
                    if (diff == 0)
                        minSkew0(i+1, v-1, v-1, List(i+1))
                    else if (diff == 1)
                        minSkew0(i+1, v-1, min, acc :+ i+1)
                    else
                        minSkew0(i+1, v-1, min, acc)
                } else if (v == min)
                    minSkew0(i+1, v, min, acc :+ i+1)
                else
                    minSkew0(i+1, v, min, acc)
            }

            minSkew0(0, 0, 0, List(0))
        }

        def distance(p: String) = {
            val l = data.length

            @tailrec
            def distance0(i: Int, acc: Int): Int =
                if (i == l)
                    acc
                else if (data(i) == p(i))
                    distance0(i+1, acc)
                else
                    distance0(i+1, acc+1)

            if (data.length != p.length)
                throw new IllegalArgumentException("Can't compute distance of string with different lengthes. " + data.length + " != " + p.length)

            distance0(0, 0)
        }

        def findApproximatePattern(p: String, errorCount: Int): List[Int] = {
            case class PossibleMatch(val start: Int, var errorCount: Int)

            def isMatch(i: Int, pm: PossibleMatch) = {
                if (data(i) == p(i - pm.start))
                    pm.errorCount <= errorCount
                else
                    pm.errorCount < errorCount
            }

            def update(i: Int, lpm: List[PossibleMatch]) = {
                lpm.foreach { pm => if (data(i) != p(i-pm.start)) pm.errorCount += 1 }
                lpm
            }

            val l = data.length
            @tailrec
            def doSearch(i: Int, q: List[PossibleMatch], acc: List[Int]): List[Int] = {
                if (i == l)
                    acc
                else if (isMatch(i, q.head))
                    doSearch(i+1, update(i, q.tail :+ PossibleMatch(i, 0)), acc :+ q.head.start)
                else
                    doSearch(i+1, update(i, q.tail :+ PossibleMatch(i, 0)), acc)
            }

            @tailrec
            def fill(i: Int, stop: Int, q: List[PossibleMatch]): List[PossibleMatch] =
                if (i == stop)
                    q
                else
                    fill(i+1, stop, update(i, q :+ PossibleMatch(i, 0)))

            doSearch(p.length-1, fill(0, p.length-1, Nil), Nil)
        }

        private def addOrGet[T, V](t: Tree[T, V], k: T)(default: => Tree[T, V]) = {
            if (!t.children.contains(k))
                t.children += k -> default
            t.children(k)
        }

        private def makeWord[T](t: Tree[Char, T]): String = {
            @tailrec
            def makeWord0[T](t: Tree[Char, T], acc: String): String =
                if (t.parent == null)
                    acc
                else
                    makeWord0(t.parent, t.k + acc)

            makeWord0(t, "")
        }

        def findClump(k: Int, windowSize: Int, threshold: Int) = {
            def doNext(c: Char)(t: Tree[Char, List[Int]]) = addOrGet(t, c) {
                new Tree[Char, List[Int]](t, mutable.Map.empty, c, Nil)
            }

            val start = new Tree[Char, List[Int]](null, mutable.Map.empty, '?', Nil)
            val l = data.length

            @tailrec
            def doFrequentWords(p: List[Tree[Char, List[Int]]], acc: Set[String], i: Int): Set[String] = {
                if (i >= l) {
                    acc
                } else {
                    p.head.v = p.head.v.filter(_ > (i - windowSize)) :+ (i - k)

                    val next = doNext(data(i)) _
                    if (p.head.v.size == threshold)
                        doFrequentWords(p.tail.map(next) :+ next(start), acc + makeWord(p.head), i + 1)
                    else
                        doFrequentWords(p.tail.map(next) :+ next(start), acc, i + 1)
                }
            }

            var p = List.empty[Tree[Char, List[Int]]]
            for (i <- 0 to k - 1) {
                val next = doNext(data(i)) _
                p = p.map(next) :+ next(start)
            }

            doFrequentWords(p, Set.empty, k)
        }

        def frequentWords(k: Int, threshold: Int = Int.MaxValue): List[(Int, String)] = {
            def doNext(c: Char)(t: Tree[Char, Int]) = addOrGet(t, c) {
                new Tree[Char, Int](t, mutable.Map.empty, c, 0)
            }

            val start = new Tree[Char, Int](null, mutable.Map.empty, '?', 0)
            val l = data.length

            @tailrec
            def doFrequentWords(p: List[Tree[Char, Int]], max: List[Tree[Char, Int]], maxV: Int, i: Int): List[Tree[Char, Int]] = {
                if (i >= l) {
                    max
                } else {
                    p.head.v += 1
                    val next = doNext(data(i)) _

                    if (threshold == Int.MaxValue) {
                        if (p.head.v == maxV) {
                            doFrequentWords(p.tail.map(next) :+ next(start), max :+ p.head, maxV, i + 1)
                        } else if (p.head.v > maxV) {
                            doFrequentWords(p.tail.map(next) :+ next(start), List(p.head), p.head.v, i + 1)
                        } else
                            doFrequentWords(p.tail.map(next) :+ next(start), max, maxV, i + 1)
                    } else {
                        if (p.head.v == threshold) {
                            doFrequentWords(p.tail.map(next) :+ next(start), max :+ p.head, maxV, i + 1)
                        } else
                            doFrequentWords(p.tail.map(next) :+ next(start), max, maxV, i + 1)
                    }
                }
            }

            if (l < k)
                Nil
            else if (l == k)
                List((1, data))
            else {
                var p = List.empty[Tree[Char, Int]]
                for (i <- 0 to k - 1) {
                    val next = doNext(data(i)) _
                    p = p.map(next) :+ next(start)
                }

                doFrequentWords(p, Nil, 0, k).map { t => (t.v, makeWord(t))}
            }
        }

        def allWordsWithErrors(k: Int, maxErrorCount: Int): Set[String] = {
            val l = data.length

            @tailrec
            def allWordsWithErrors0(current: List[(Int, String)], acc: Set[String], i: Int): Set[String] =
                if (l == i) {
                    acc ++ current.takeWhile(_._2.length == k).map(_._2)
                } else {
                    val ch = data(i).toString
                    val (newWords, rest) = current.divide(_._2.length == k)
                    val newCurrent = rest.flatMap { case (errorCount, word) =>
                        if (errorCount == maxErrorCount) {
                            List((errorCount, word + ch))
                        } else {
                            DNA_ALFABET.filter(_ != ch).map(ch => (errorCount+1, word + ch)) :+
                                (errorCount, word + ch)
                        }
                    }

                    val fresh = if (maxErrorCount > 0)
                        DNA_ALFABET.filter(_ != ch).map(ch => (1, ch)) :+ (0, ch)
                    else List((0, ch))

                    allWordsWithErrors0(newCurrent ++ fresh, acc ++ newWords.map(_._2), i+1)
                }

            allWordsWithErrors0(Nil, Set.empty, 0)
        }

        def frequentWordsWithErrors(k: Int, errorCount: Int): List[String] = {
            val l = data.length

            val start = new Tree[Char, Int](null, mutable.Map.empty, '?', 0)

            def doNext(c: Char)(t: Tree[Char, Int], h: Int) = {
                val newT = addOrGet(t, c) {
                    new Tree[Char, Int](t, mutable.Map.empty, c, 0)
                }

                if (h+1 == k)
                    newT.v += 1
                newT
            }

            def doNextWithErrors(ch: Char, twe: TreeWithError) = {
                val newWithErrors = List('A', 'C', 'T', 'G').filter(_ != ch).map { ch =>
                    TreeWithError(tree = doNext(ch)(twe.tree, twe.height),
                        errorCount = twe.errorCount+1, height = twe.height+1)
                }

                twe.tree = doNext(ch)(twe.tree, twe.height)
                twe.height += 1

                newWithErrors :+ twe
            }

            def next(i: Int, p: List[TreeWithError]): List[TreeWithError] = {
                val ch = data(i)

                p.map { twe =>
                    if (twe.errorCount == errorCount) {
                        twe.tree = doNext(ch)(twe.tree, twe.height)
                        twe.height += 1
                        List(twe)
                    } else
                        doNextWithErrors(ch, twe)
                }.flatten ++ doNextWithErrors(ch, TreeWithError(0, 0, start))
            }

            @tailrec
            def fwwe(i: Int, p: List[TreeWithError], max: Int, acc: List[Tree[Char, Int]]): List[Tree[Char, Int]] = {
                if (i == l)
                    acc
                else {
                    val n = next(i, p)
                    val (drop, leave) = n.divide(_.height == k)
                    val (morethenmax, equalmax) = drop.filter(_.tree.v >= max).map(_.tree).divide(_.v > max)
                    if (morethenmax.isEmpty) {
                        fwwe(i+1, leave, max, acc ++ equalmax)
                    } else {
                        val newmax = morethenmax.head.v
                        fwwe(i+1, leave, newmax, morethenmax)
                    }
                }
            }

            val res = fwwe(0, Nil, 0, Nil)
            res.map(makeWord)
        }

        def frequentWordsWithErrorsAndReverse(k: Int, errorCount: Int): List[String] = {
            val l = data.length

            val start = new Tree[Char, Int](null, mutable.Map.empty, '?', 0)

            def doNext(c: Char)(t: Tree[Char, Int], h: Int) = {
                val newT = addOrGet(t, c) {
                    new Tree[Char, Int](t, mutable.Map.empty, c, 0)
                }

                if (h+1 == k)
                    newT.v += 1
                newT
            }

            def doNextWithErrors(ch: Char, twe: TreeWithError) = {
                val newWithErrors = List('A', 'C', 'T', 'G').filter(_ != ch).map { ch =>
                    TreeWithError(tree = doNext(ch)(twe.tree, twe.height),
                        errorCount = twe.errorCount+1, height = twe.height+1)
                }

                twe.tree = doNext(ch)(twe.tree, twe.height)
                twe.height += 1

                newWithErrors :+ twe
            }

            def next(i: Int, p: List[TreeWithError]): List[TreeWithError] = {
                val ch = data(i)

                p.map { twe =>
                    if (twe.errorCount == errorCount) {
                        twe.tree = doNext(ch)(twe.tree, twe.height)
                        twe.height += 1
                        List(twe)
                    } else
                        doNextWithErrors(ch, twe)
                }.flatten ++ doNextWithErrors(ch, TreeWithError(0, 0, start))
            }

            @tailrec
            def fwwe(i: Int, p: List[TreeWithError], acc: List[Tree[Char, Int]]): List[Tree[Char, Int]] =
                if (i == l)
                    acc
                else {
                    val n = next(i, p)
                    val (drop, leave) = n.divide(_.height == k)
                    val newwords = drop.filter(_.tree.v == 1).map(_.tree)
                    fwwe(i+1, leave, acc ++ newwords)
                }

            val wordFrequencyWithErrorsTree = fwwe(0, Nil, Nil)
            val wordFrequencyWithErrorsMap = wordFrequencyWithErrorsTree.map(x => (makeWord(x), x.v)).toMap

            wordFrequencyWithErrorsMap.foldLeft((0, List[String]())) { (c, t) =>
                val count = if (wordFrequencyWithErrorsMap.contains(t._1.reverseComplement))
                    t._2 + wordFrequencyWithErrorsMap(t._1.reverseComplement)
                else
                    t._2

                if (count > c._1) {
                    (count, List(t._1))
                } else if (count == c._1)
                    (c._1,  List(t._1) ++ c._2)
                else
                    c
            }._2
        }

        def findAmino(amino: String): List[String] = {
            val aminoDNA = amino.peptid2rna.map(_.toDNA)
            val patterns = (aminoDNA ++ aminoDNA.map(_.reverseComplement))

            patterns match {
                case Nil => Nil
                case h :: tail => {
                    val patternsSet = patterns.toSet
                    val l = data.length
                    val pl = h.length
                    @tailrec
                    def findPatternIndexes(i: Int, acc: List[String]): List[String] = {
                        if (i <= l) {
                            val substr = data.substring(i-pl, i)
                            if (patternsSet(substr))
                                findPatternIndexes(i+1, acc :+ substr)
                            else
                                findPatternIndexes(i+1, acc)
                        } else acc
                    }

                    findPatternIndexes(pl, Nil)
                }
            }
        }

        def subPeptides: Seq[String] = {
            def subPeptides0(data: String, l: Int): Seq[String] = {
                val dl = data.length
                if (l >= dl)
                    List(data)
                else  (0 to data.length-1).map { i =>
                    if (i+l < data.length)
                        data.substring(i, i+l)
                    else {
                        data.substring(i) + data.substring(0, l - (dl - i))
                    }
                }
            }

            (1 to data.length).flatMap { l => subPeptides0(data, l) }
        }

        def spectrum: List[Int] = List(0) ++ data.subPeptides.map(_.peptideMass).sorted

        def matchScoring(spectrum: List[Int]): Int = {
            @tailrec
            def matchScoring0(ts: List[Int], es: List[Int], acc: Int): Int = ts match {
                case Nil => acc
                case tsh :: tst => {
                    es match {
                        case Nil => acc
                        case esh :: est => if (tsh == esh)  matchScoring0(tst, est, acc+1)
                        else if (tsh > esh)
                            matchScoring0(ts, est, acc)
                        else
                            matchScoring0(tst, es, acc)
                    }
                }
            }
            matchScoring0(data.spectrum, spectrum.sorted, 0)
        }

        def greedyMotifSearch(k: Int, profile: Map[Char, List[Double]]): String = {
            def propability(i: Int): Double =
                (0 to k-1).map(j => profile(data(i+j))(j)).foldLeft(1d)((a,b)=>a*b)

            val dl = data.length
            @tailrec
            def greedyMotifSearch0(i: Int, max: (Int, Double)): Int = {
                if (i+k == data.length)
                    max._1
                else {
                    val p = propability(i)
                    if (p > max._2)
                        greedyMotifSearch0(i+1, (i, p))
                    else
                        greedyMotifSearch0(i+1, max)
                }
            }

            val x = greedyMotifSearch0(0, (0, 0))
            data.substring(x, x+k)
        }

        def substrSet(l: Int): SortedSet[String] = {
            val dl = data.length
            @tailrec
            def substrSet0(i: Int, acc: SortedSet[String]): SortedSet[String] =
                if (i+l <= dl)
                    substrSet0(i+1, acc + data.substring(i, i+l))
                else acc

            substrSet0(0, SortedSet[String]())
        }

        def isEndsWith(start: Int, other: String): Boolean = {
            @tailrec
            def isEndsWith0(i: Int, cnt: Int): Boolean =
                if (i == cnt)
                    true
                else if (data(start+i) != other(i))
                    false
                else
                    isEndsWith0(i+1, cnt)

            isEndsWith0(0, data.length-start)
        }

        def minOverlapIndex(other: String): Int = {
            val dl = data.length

            @tailrec
            def minOverlapIndex0(i: Int): Int =
                if (i == 0)
                    0
                else if (data.isEndsWith(dl-i, other))
                    i
                else
                    minOverlapIndex0(i-1)

            minOverlapIndex0(dl)
        }

        def overlap(other: String) = data + other.substring(data.minOverlapIndex(other))

        def deBrujinGraph(k: Int): Map[String, List[String]] = {
            val dl = data.length
            @tailrec
            def graph(i: Int, acc: Map[String, List[String]]): Map[String, List[String]] = if (i+k <= dl) {
                val prefix = data.substring(i, i+k-1)
                val suffix = data.substring(i+1, i+k)

                val item = prefix -> (suffix :: acc.getOrElse(prefix, List()))
                graph(i+1, acc + item)
            } else acc

            graph(0, Map())
        }

        def editDistance(other: String): Int =  {
            def min(nums: Int*): Int = nums.min
            val lenStr1 = data.length
            val lenStr2 = other.length

            val d: Array[Array[Int]] = Array.ofDim(lenStr1 + 1, lenStr2 + 1)

            for (i <- 0 to lenStr1) d(i)(0) = i
            for (j <- 0 to lenStr2) d(0)(j) = j

            for (i <- 1 to lenStr1; j <- 1 to lenStr2) {
                val cost = if (data(i - 1) == other(j-1)) 0 else 1
                d(i)(j) = min( d(i-1)(j  ) + 1, d(i  )(j-1) + 1, d(i-1)(j-1) + cost)
            }

            d(lenStr1)(lenStr2)
        }

        def kmers(k: Int): Map[String, List[Int]] = {
            @tailrec
            def kmers0(i: Int, acc: Map[String, List[Int]]): Map[String, List[Int]] = if (i + k <= data.length){
                val kmer = data.substring(i, i+k)
                val entry = if (acc.contains(kmer)) kmer -> (acc(kmer) :+ i) else kmer -> List(i)
                kmers0(i+1, acc + entry)
            }  else acc

            kmers0(0, SortedMap())
        }
    }

    implicit class IterableExt[E](data: Iterable[E]) {
        def divide(p: (E) => Boolean) = data.foldLeft((List.empty[E], List.empty[E])) { case (l_r, e) =>
            if (p(e))
                (l_r._1 :+ e, l_r._2)
            else
                (l_r._1, l_r._2 :+ e)
        }
    }

    val peptideWeights = List(57, 71, 87, 97, 99, 101, 103, 113, 114, 115, 128, 129, 131, 137, 147, 156, 163, 186).reverse

    implicit class ListIntExt(data: List[Int]) {
        def spectrum: Seq[Int] = spectrum(true)
        def spectrum(genCycleSums: Boolean = true): Seq[Int] = {
            def spectrum0(l: Int): Seq[Int] = {
                val dl = data.length
                if (l >= dl)
                    Seq(data.sum)
                else  (0 to data.length-1).map { i =>
                    if (i+l < data.length) {
                        var sum = 0
                        (i to i+l-1).foreach { i =>
                            sum += data(i)
                        }
                        sum
                    } else if (!genCycleSums)
                        -1
                    else {
                        var sum = 0
                        (i to dl-1).foreach { i => sum += data(i) }
                        (0 to l - (dl-i) - 1).foreach { i => sum += data(i) }
                        sum
                    }
                }
            }

            (2 to data.length).flatMap { l => spectrum0(l) }.filter(_ != -1)
        }

        def sequencePeptide: List[List[Int]] = {
            case class Peptide(mass: Int = 0, seq: List[Int] = List[Int](),
                                      map: Map[Int, Int] = Map[Int, Int](), spectrum: List[Int] = List[Int]())

            val allowedWeights = peptideWeights.filter(data.contains(_))
            val parentMass = data.last
            val weights = TreeMap[Int, Int]() ++ data.groupBy(identity).mapValues(_.length)

            def expand(peptides: List[Peptide]): List[Peptide] = peptides.flatMap { p =>
                allowedWeights.filter(w => p.map.getOrElse(w, 0)+1 <= weights.getOrElse(w, 0)).map { w =>
                   val nseq = p.seq :+ w
                   val spectrum = (nseq ++ nseq.spectrum(false)).sorted
                    p.copy(p.mass+w, nseq, p.map.updated(w, p.map.getOrElse(w, 0) + 1), spectrum = spectrum)
                }.filter { p => !p.spectrum.find(!weights.contains(_)).isDefined }
            }

            def check(p: Peptide) =
                (List(0) ++ p.seq ++ p.seq.spectrum).sorted == data

            @tailrec
            def sp(c: List[Peptide], acc: List[Peptide]): List[Peptide] = c match {
                case Nil => acc
                case c => {
                    val expanded = expand(c)
                    val (nc, nacc) = expanded.divide(_.mass != parentMass)
                    sp(nc, acc ++ nacc.filter(check))
                }
            }

            sp(List(Peptide()), Nil).map(_.seq.toList)
        }

        def leaderBoardCyclopeptideSequencing(boardSize: Int, allowedWeights: List[Int] = peptideWeights): List[List[Int]] = {
            case class Peptide(mass: Int = 0, score: Int = 0, seq: List[Int] = Nil, spectrum: List[Int] = Nil)

            val parentMass = data.last
            val weights = TreeMap[Int, Int]() ++ data.groupBy(identity).mapValues(_.length)

            def score(spectrum: List[Int]): Int = {
                val spectrumMap = mutable.Map[Int, Int]()
                spectrum.foreach { mass =>
                    if (spectrumMap.contains(mass))
                        spectrumMap.update(mass, spectrumMap(mass)+1)
                    else
                        spectrumMap.update(mass, 1)
                }

                spectrumMap.keySet.foldLeft(0) { case (score, mass) =>
                    val count = spectrumMap(mass)
                    if (weights.contains(mass)) {
                        score + Math.min(weights(mass), count)
                    } else
                        score
                }
            }

            def expand(board: List[Peptide]): List[Peptide] = board.flatMap { p =>
                allowedWeights.filter(_ + p.mass <= parentMass).map { w =>
                    val nseq = p.seq :+ w
                    val spectrum = (0 to nseq.length-1).foldLeft(p.spectrum) { (s, i) =>
                        var sum = 0
                        (i to nseq.length-1).foreach { j =>
                            sum += nseq(j)
                        }
                        s :+ sum
                    }
                    val s = score(spectrum)
                    p.copy(mass = p.mass + w, seq = nseq, score = s, spectrum = spectrum)
                }
            }

            def trim(board: List[Peptide]): List[Peptide] = {
                var count = 0
                var currentScore = -1
                val sorted = board.sortBy(-_.score)

                sorted.takeWhile { p =>
                    var changed = false
                    if (p.score != currentScore) {
                        currentScore = p.score
                        changed = true
                    }

                    count += 1
                    count <= boardSize || !changed
                }
            }

            def lbcq(board: List[Peptide], leaders: (Int, List[Peptide]), i: Int): List[Peptide] = board match {
                case Nil => leaders._2
                case board => {
                    val expanded = expand(board)
                    val (newBoard, possibleLeaders) = expanded.divide(_.mass != parentMass)

                    val newLeaders = possibleLeaders.foldLeft(leaders) { case ((maxScore, leaders), p) =>
                        val s = score(p.seq ++ p.seq.spectrum(true))
                        if (s > maxScore)
                            (s, List(p.copy(score = s)))
                        else if (s == maxScore)
                            (maxScore, leaders :+ p.copy(score = s))
                        else
                            (maxScore, leaders)
                    }
                    lbcq(trim(newBoard), newLeaders, i+1)
                }
            }

            lbcq(List(Peptide()), (-1, Nil), 0).map(_.seq.toList)
        }

        sealed case class Item(value: Int, i: Int, j: Int, var count: Int = 1)
        def convolution0: Map[Int, Item] = {
            (0 to data.length - 1).foldLeft(Map[Int, Item]()) { (map, i) =>
                (0 to data.length - 1).foldLeft(map) { (map, j) =>
                    val key = data(i) - data(j)
                    if (key > 0) {
                        if (map.contains(key)) {
                            map(key).count += 1
                            map
                        } else
                            map.updated(key, Item(key, i, j))
                    } else
                        map
                }
            }
        }

        def convolution: List[Int] =
            convolution0.values.toList.sortBy(item => (-item.count, item.j, item.i)).flatMap(item => List.fill(item.count)(item.value))

        def convolutionCyclopeptideSequence(m: Int, n: Int): List[List[Int]] = {
            val fullConvolution = convolution0.values.filter(x => x.value >= 57 && x.value <= 200).toList.sortBy(-_.count)
            val trimmedConvolution = fullConvolution.foldLeft((-1, 0, List[Int]())) { (state, item) =>
                if (state._2 > m && state._1 != item.count)
                    state
                else if (item.count == state._1)
                    (state._1, state._2+1, state._3 :+ item.value)
                else
                    (item.count, state._2+1, state._3 :+ item.value)
            }._3

            data.sorted.leaderBoardCyclopeptideSequencing(n, trimmedConvolution)
        }
    }

    implicit class ListOfListsExt[A](data: Iterable[Iterable[A]]) {
        def intersection: Set[A] = data match {
            case Nil => Set.empty[A]
            case h :: t => t.foldLeft(h.toSet) { (set, item) => set.intersect(item.toSet) }
        }
    }

    implicit class ListStringExt(data: List[String]) {
        def motifEnumeration(k: Int, d: Int): List[String] =
            data.map(str => str.allWordsWithErrors(k, d)).intersection.toList.sorted

        def motifMedian(k: Int): List[String] = {
            def distance(dna: String, pattern: String, start: Int) = {
                val pl = pattern.length
                @tailrec
                def distance0(acc: Int, i: Int): Int = {
                    if (pl == i)
                        acc
                    else if (dna(start + i) == pattern(i))
                        distance0(acc, i + 1)
                    else
                        distance0(acc + 1, i + 1)
                }

                distance0(0, 0)
            }

            k.kmers.foldLeft((k + 1, List(""))) { case ((min, answer), pattern) =>
                if (min == 0)
                    (min, answer)
                else {
                    val distance2pattern = data.map { dna =>
                        (0 to dna.length - pattern.length).map(distance(dna, pattern, _)).min
                    }.sum

                    if (min > distance2pattern)
                        (distance2pattern, List(pattern))
                    else if (min == distance2pattern)
                        (distance2pattern, answer :+ pattern)
                    else (min, answer)
                }
            }._2
        }

        def greedyMotifSearch(k: Int, t: Int): List[String] = ???

        def reconstruct: String = {
            val dl = data.length

            @tailrec
            def reconstruct0(i: Int, acc: String): String =
                if (i == dl) acc else reconstruct0(i+1, acc.overlap(data(i)))

            if (dl == 0)
                ""
            else
                reconstruct0(1, data(0))
        }

        def overlapGraph: Map[String, String] = {
            val dl = data.length
            @tailrec
            def makeSuffixes(i: Int, prefixes: Map[String, Int], suffixes: Map[String, Int]): (Map[String, Int], Map[String, Int]) =
                if (i != dl ) {
                    val suffix = data(i).substring(1)
                    val prefix = data(i).substring(0, data(i).length-1)
                    makeSuffixes(i+1, prefixes + (prefix -> i), suffixes + (suffix -> i))
                } else (prefixes, suffixes)

            val (prefixes, suffixes) = makeSuffixes(0, Map(), Map())

            @tailrec
            def findPairs(i: Int, prefixes: Map[String, Int], suffixes: Map[String, Int], acc: Map[String, String]): Map[String, String] = if (i != dl) {
                val prefix = data(i).substring(0, data(i).length-1)
                val suffix = data(i).substring(1)

                if (suffixes.contains(prefix) && suffixes(prefix) != i) {
                    val first = data(suffixes(prefix))
                    findPairs(i+1, prefixes - prefix, suffixes - prefix, acc + (first -> data(i)))
                } else if (prefixes.contains(suffix) && prefixes(suffix) != i) {
                    val second = data(prefixes(suffix))
                    findPairs(i+1, prefixes - suffix, suffixes - suffix, acc + (data(i) -> second))
                } else findPairs(i+1, prefixes, suffixes, acc)
            } else acc

            findPairs(0, prefixes, suffixes, Map())
        }

        def deBrujinGraph: Map[String, List[String]] = {
            val dl = data.length
            @tailrec
            def deBrujinGraph0(i: Int, acc: Map[String, List[String]]): Map[String, List[String]] = if (i < dl) {
                val prefix = data(i).substring(0, data(i).length-1)
                val suffix = data(i).substring(1)

                val item = prefix -> (suffix :: acc.getOrElse(prefix, List()))

                deBrujinGraph0(i+1, acc + item)
            } else acc

            deBrujinGraph0(0, Map())
        }

    }

    def subPeptideCount(n: Int): Int = n*(n-1)

    def eulerianCycle[P](data: Map[P, List[P]]): List[P] = {
        @tailrec
        def eulerianCycle0(stack: List[P], location: P, circuit: List[P], data: Map[P, List[P]]): List[P] = {
            if (!data(location).isEmpty) {
                val nextLocation = data(location).head
                val newData =  if (data(location).isEmpty) data else data + (location -> data(location).tail)
                eulerianCycle0(location :: stack, nextLocation, circuit, newData)
            } else if (!stack.isEmpty) {
                eulerianCycle0(stack.tail, stack.head, circuit :+ location, data)
            } else (circuit :+ location).reverse
        }

        val cycle = eulerianCycle0(List(), data.head._1, List(), data)
        cycle
    }

    def eulerianPath[P](data: Map[P, List[P]]): List[P] = {
        @tailrec
        def eulerianCycle0(stack: List[P], location: P, circuit: List[P], data: Map[P, List[P]]): List[P] = {
            if (data.contains(location) && !data(location).isEmpty) {
                val nextLocation = data(location).head
                val newData =  if (data(location).isEmpty) data else data + (location -> data(location).tail)

                eulerianCycle0(location :: stack, nextLocation, circuit, newData)
            } else if (!stack.isEmpty) {
                eulerianCycle0(stack.tail, stack.head, circuit :+ location, data)
            } else circuit.reverse
        }

        val l = data.foldLeft(0) {(sum, x) => sum + x._2.length }

        @tailrec
        def eulerianPath0(seq: Map[P, List[P]]): List[P] = if (!seq.isEmpty) {
            val cycle = seq.head._1 :: eulerianCycle0(List(), seq.head._1, List(), data)
            if (cycle.length == l+1)
                cycle
            else
                eulerianPath0(seq.tail)
        } else Nil

        eulerianPath0(data)
    }

    implicit class BioInfInt2(n: Int) {
        def universalCircular: String = {
            @tailrec
            def kstr(k: Int, acc: List[String]): List[String] = if (k != 0) {
                kstr(k-1, acc.flatMap( s => List(s + "1", s + "0")))
            } else acc

            val graph = SortedMap[String, List[String]]() ++ kstr(n-1, List("1", "0")).map { kmer =>
                val prefix = kmer.substring(1)
                val common = kmer.substring(2)
                (prefix, List(common + "1", common + "0"))
            }.toMap

            @tailrec
            def combine(s: String, t: List[String], acc: List[String]): List[String] = if(!t.isEmpty) {
                if (s == "")
                    combine(t.head, t.tail, acc)
                else
                    combine(t.head, t.tail, acc :+ (s.substring(0, 1) + t.head))
            } else acc

            val sc = eulerianCycle(graph)
            val sourceCycle = sc.last :: sc.reverse.tail.reverse
            val cycle = combine(sourceCycle.head, sourceCycle.tail, List()).reverse.drop(n-1).reverse
            cycle.tail.foldLeft(cycle.head) { (res, el) =>
                if (res.indexOf(el) == -1) {
                    res + el.last
                } else res
            }
        }
    }

    def manhattanTouristPath(m: Int, n: Int, down: Array[Array[Int]], right: Array[Array[Int]]): Int = {
        val s = Array.fill(n+1, m+1)(0)
        s(0)(0) = 0

        (1 to n).foreach(i => s(i)(0) = s(i-1)(0) + down(i-1)(0))
        (1 to m).foreach(j => s(0)(j) = s(0)(j-1) + right(0)(j-1))

        (1 to n).foreach { i =>
            (1 to m).foreach { j =>
                s(i)(j) = Math.max(s(i-1)(j) + down(i-1)(j), s(i)(j-1) + right(i)(j-1))
            }
        }

        s(n)(m)
    }

    def outputLCS(v: String, w: String) = {
        val RIGHT = 0
        val DOWN = 1
        val DOWN_RIGHT = 2

        def backtrack(v: String, w: String): Array[Array[Int]]  = {
            val s = Array.fill(v.length+1, w.length+1)(0)
            val backtrack = Array.fill(v.length+1, w.length+1)(0)

            (0 to v.length).foreach(i => s(i)(0) = 0)
            (0 to w.length).foreach(j => s(0)(j) = 0)

            (1 to v.length).foreach { i =>
                (1 to w.length).foreach { j =>
                    s(i)(j) = if (s(i-1)(j) == s(i)(j-1)) {
                        if (v(i-1) == w(j-1)) s(i-1)(j-1)+1 else s(i-1)(j)
                    } else Math.max(s(i-1)(j), s(i)(j-1))

                    backtrack(i)(j) = if (s(i)(j) == s(i-1)(j))
                        DOWN
                    else if (s(i)(j) == s(i)(j-1))
                        RIGHT
                    else
                        DOWN_RIGHT
                }
            }

            backtrack
        }

        def outputLCS0(backtrack: Array[Array[Int]], v: String, i: Int, j: Int): String = {
            if (i == 0 || j == 0)
                ""
            else if (backtrack(i)(j) == DOWN)
                outputLCS0(backtrack, v, i-1, j)
            else if (backtrack(i)(j) == RIGHT)
                outputLCS0(backtrack, v, i, j-1)
            else
                outputLCS0(backtrack, v, i-1, j-1) + v(i-1)
        }

        outputLCS0(backtrack(v, w), v, v.length, w.length)
    }

    def findLongestPath(start: Int, end: Int, graph: Map[Int, List[(Int, Int)]]): (Int, List[Int]) = {
        def findLongestPath0(start: Int, end: Int, graph: Map[Int, List[(Int, Int)]], acc: (Int, List[Int])): (Int, List[Int]) = if (start != end) {
            if (graph.contains(start)) {
                graph(start).map{ case (next, weight) =>
                    findLongestPath0(next, end, graph, (acc._1 + weight, acc._2 :+ next))
                }.maxBy(_._1)
            } else {
                (-1, Nil)
            }
        } else acc

        findLongestPath0(start, end, graph, (0, List(start)))
    }

    def localAlignment(v: String, w: String, m: Map[Char, Map[Char, Int]], sigma: Int): (Int, String, String) = {
        def max4(i: Int, j: Int, k: Int, l: Int) = Math.max(Math.max(i, Math.max(j, k)), l)

        val s = Array.fill(v.length+1)(Array.fill(w.length+1)(0))

        var maxIJ = (0, 0)
        (1 to v.length).foreach { i =>
            (1 to w.length).foreach { j =>
                s(i)(j) = max4(0, s(i-1)(j) - sigma, s(i)(j-1) - sigma, s(i-1)(j-1) + m(v(i-1))(w(j-1)))
                if (s(maxIJ._1)(maxIJ._2) < s(i)(j))
                    maxIJ = (i, j)
            }
        }

        def backtrack(i: Int, j: Int, v1: String, w1: String): (String, String) = if (i==0 && j==0) {
            (v1, w1)
        } else {
            if (s(i)(j) <= 0)
                (v1, w1)
            else if (i == 0)
                (v1, w1)
            else if (j == 0)
                (v1, w1)
            else {
                val x = m(v(i-1))(w(j-1))
                if (s(i-1)(j)-sigma == s(i)(j)) {
                    backtrack(i-1, j, v(i-1)+v1, "-"+w1)
                } else if (s(i)(j-1)-sigma == s(i)(j)) {
                    backtrack(i, j-1, "-"+v1, w(j-1)+w1)
                } else {
                    backtrack(i-1, j-1, v(i-1)+v1, w(j-1)+w1)
                }
            }
        }

        val (v1, w1) = backtrack(maxIJ._1, maxIJ._2, "", "")
        (s(maxIJ._1)(maxIJ._2), v1, w1)
    }

    def globalAlignment(v: String, w: String, m: Map[Char, Map[Char, Int]], sigma: Int): (Int, String, String) = {
        def max3(i: Int, j: Int, k: Int) = Math.max(i, Math.max(j, k))

        val s = Array.fill(v.length+1)(Array.fill(w.length+1)(0))
        (1 to v.length).foreach {i => s(i)(0) = -sigma*i }
        (1 to w.length).foreach {i => s(0)(i) = -sigma*i }

        (1 to v.length).foreach { i =>
            (1 to w.length).foreach { j =>
                s(i)(j) = max3(s(i-1)(j) - sigma, s(i)(j-1) - sigma, s(i-1)(j-1) + m(v(i-1))(w(j-1)))
            }
        }

        def backtrack(i: Int, j: Int, v1: String, w1: String): (String, String) = if (i==0 && j==0) {
            (v1, w1)
        } else {
            if (i == 0)
                backtrack(i, j-1, "-"+v1, w(j-1)+w1)
            else if (j==0)
                backtrack(i-1, j, v(i-1)+v1, "-"+w1)
            else {
                val x = m(v(i-1))(w(j-1))
                if (s(i)(j-1)-sigma == s(i)(j)) {
                    backtrack(i, j-1, "-"+v1, w(j-1)+w1)
                } else if (s(i-1)(j)-sigma == s(i)(j)) {
                    backtrack(i-1, j, v(i-1)+v1, "-"+w1)
                } else /*if (s(i-1)(j-1)+x == s(i)(j))*/ {
                    backtrack(i-1, j-1, v(i-1)+v1, w(j-1)+w1)
                }
            }
        }

        val (v1, w1) = backtrack(v.length, w.length, "", "")
        (s(v.length)(w.length), v1, w1)
    }

    def fittingAlignment(v: String, w: String, sigma: Int): (Int, String, String) = {
        def max3(i: Int, j: Int, k: Int) = Math.max(i, Math.max(j, k))

        val s = Array.fill(v.length+1)(Array.fill(w.length+1)(0))

        var score = Int.MinValue
        var maxIJ = (0, 0)
        (1 to v.length).foreach { i =>
            (1 to w.length).foreach { j =>
                val x = if (v(i-1) == w(j-1)) 1 else -1
                s(i)(j) = max3(s(i-1)(j) - sigma, s(i)(j-1) - sigma, s(i-1)(j-1) + x)
                if (i >= w.length && j >= w.length) {
                    if (score < s(i)(j)) {
                        score = s(i)(j)
                        maxIJ = (i, j)
                    }
                }
            }
        }

        def backtrack(i: Int, j: Int, v1: String, w1: String): (String, String) = if (i==0 && j==0) {
            (v1, w1)
        } else {
            if (i == 0)
                (v1, w1)
            else if (j == 0)
                (v1, w1)
            else {
                val x = 1
                if (s(i-1)(j)-sigma == s(i)(j)) {
                    backtrack(i-1, j, v(i-1)+v1, "-"+w1)
                } else if (s(i)(j-1)-sigma == s(i)(j)) {
                    backtrack(i, j-1, "-"+v1, w(j-1)+w1)
                } else {
                    backtrack(i-1, j-1, v(i-1)+v1, w(j-1)+w1)
                }
            }
        }

        val (v1, w1) = backtrack(maxIJ._1, maxIJ._2, "", "")
        (s(maxIJ._1)(maxIJ._2), v1, w1)
    }

    def overlapAlignment(v: String, w: String, sigma: Int): (Int, String, String) = {
        def max3(i: Int, j: Int, k: Int) = Math.max(i, Math.max(j, k))

        val s = Array.fill(v.length+1)(Array.fill(w.length+1)(0))

        var score = Int.MinValue
        var maxIJ = (0, 0)
        (1 to v.length).foreach { i =>
            (1 to w.length).foreach { j =>
                val x = if (v(i-1) == w(j-1)) 1 else -sigma
                s(i)(j) = max3(s(i-1)(j) - sigma, s(i)(j-1) - sigma, s(i-1)(j-1) + x)
                if (i == v.length) {
                    if (score <= s(i)(j)) {
                        score = s(i)(j)
                        maxIJ = (i, j)
                    }
                }
            }
        }

        def backtrack(i: Int, j: Int, v1: String, w1: String): (String, String) = if (i==0 && j==0) {
            (v1, w1)
        } else {
            if (i == 0)
                backtrack(i-1, j, v(i-1)+v1, "-"+w1)
            else if (j==0)
                (v1, w1)
            else {
                val x = if (v(i-1) == w(j-1)) 1 else -2
                if (s(i-1)(j-1)+x == s(i)(j)) {
                    backtrack(i-1, j-1, v(i-1)+v1, w(j-1)+w1)
                } else if (s(i)(j-1)-sigma == s(i)(j)) {
                    backtrack(i, j-1, "-"+v1, w(j-1)+w1)
                } else {
                    backtrack(i-1, j, v(i-1)+v1, "-"+w1)
                }
            }
        }


        val (v1, w1) = backtrack(maxIJ._1, maxIJ._2, "", "")
        (s(maxIJ._1)(maxIJ._2), v1, w1)
    }

    def affineGapPenaltiesAlignment(v: String, w: String, m: Map[Char, Map[Char, Int]], sigma: Int, epsilon: Int): (Int, String, String) = {
        val UP_LEFT = 1
        val LEFT = 2
        val UP = 3

        def max3(up: Int, left: Int, up_left: Int): (Int, Int) = {
            if (up > left && up > up_left)
                (up, UP)
            else if (left > up_left)
                (left, LEFT)
            else
                (up_left, UP_LEFT)
        }

        val s = Array.fill(v.length+1)(Array.fill(w.length+1)(0))
        val a = Array.fill(v.length+1)(Array.fill(w.length+1)(0))
        (1 to v.length).foreach {i => s(i)(0) = -epsilon*(i-1) - sigma; a(i)(0) = UP }
        (1 to w.length).foreach {i => s(0)(i) = -epsilon*(i-1) - sigma; a(0)(i) = LEFT }

        (1 to v.length).foreach { i =>
            (1 to w.length).foreach { j =>
                val x = if (a(i-1)(j) == UP) epsilon else sigma
                val y = if (a(i)(j-1) == LEFT) epsilon else sigma
                val (sij, aij) = max3(s(i-1)(j) - x, s(i)(j-1) - y, s(i-1)(j-1) + m(v(i-1))(w(j-1)))
                s(i)(j) = sij
                a(i)(j) = aij
            }
        }

        def NO_GAP = 0
        def LEFT_GAP = 1
        def UP_GAP = 2

        def backtrack(i: Int, j: Int, v1: String, w1: String, gap: Int): (String, String) = if (i==0 && j==0) {
            (v1, w1)
        } else {
            if (i == 0)
                backtrack(i, j-1, "-"+v1, w(j-1)+w1, LEFT_GAP)
            else if (j==0)
                backtrack(i-1, j, v(i-1)+v1, "-"+w1, UP_GAP)
            else {
                if (gap == LEFT_GAP) {
                    if (s(i)(j-1)-sigma == s(i)(j))
                        backtrack(i, j - 1, "-" + v1, w(j - 1) + w1, NO_GAP)
                    else
                        backtrack(i, j - 1, "-" + v1, w(j - 1) + w1, LEFT_GAP)
                } else if (gap == UP_GAP) {
                    if (s(i-1)(j)-sigma == s(i)(j))
                        backtrack(i-1, j, v(i-1)+v1, "-"+w1, NO_GAP)
                    else
                        backtrack(i-1, j, v(i-1)+v1, "-"+w1, UP_GAP)

                } else {
                    val x = m(v(i-1))(w(j-1))
                    if (s(i-1)(j-1)+x == s(i)(j)) {
                        backtrack(i-1, j-1, v(i-1)+v1, w(j-1)+w1, NO_GAP)
                    } else if (s(i)(j-1)-sigma == s(i)(j) || s(i)(j-1)-epsilon == s(i)(j)) {
                        backtrack(i, j - 1, "-" + v1, w(j - 1) + w1, if (s(i)(j-1)-sigma==s(i)(j)) NO_GAP else LEFT_GAP)
                    } else {
                        backtrack(i-1, j, v(i-1)+v1, "-"+w1, if (s(i-1)(j)-sigma==s(i)(j)) NO_GAP else UP_GAP)
                    }
                }

            }
        }

        val (v1, w1) = backtrack(v.length, w.length, "", "", NO_GAP)
        (s(v.length)(w.length), v1, w1)
    }

    def findMiddleEdge(v: String, w: String, m: Map[Char, Map[Char, Int]], sigma: Int): List[(Int, Int)] = {
        def max3(i: Int, j: Int, k: Int) = Math.max(i, Math.max(j, k))

        val s = Array.fill(v.length+1)(Array.fill(w.length+1)(0))
        (1 to v.length).foreach {i => s(i)(0) = -sigma*i }
        (1 to w.length).foreach {i => s(0)(i) = -sigma*i }

        (1 to v.length).foreach { i =>
            (1 to w.length).foreach { j =>
                s(i)(j) = max3(s(i-1)(j) - sigma, s(i)(j-1) - sigma, s(i-1)(j-1) + m(v(i-1))(w(j-1)))
            }
        }

        var previous = (0, 0)
        var middle = (0, 0)

        val mc = w.length/2
        var mr = 0

        def backtrack(i: Int, j: Int, v1: String, w1: String): (String, String) = if (i==0 && j==0) {
            (v1, w1)
        } else {
            if (mr == 0) {
                if (j == mc) {
                    mr = i
                } else {
                    previous = (i, j)
                }
            }

            if (i == 0)
                backtrack(i, j-1, "-"+v1, w(j-1)+w1)
            else if (j==0)
                backtrack(i-1, j, v(i-1)+v1, "-"+w1)
            else {
                val x = m(v(i-1))(w(j-1))
                if (s(i-1)(j-1)+x == s(i)(j)) {
                    backtrack(i-1, j-1, v(i-1)+v1, w(j-1)+w1)
                } else if (s(i)(j-1)-sigma == s(i)(j)) {
                    backtrack(i, j-1, "-"+v1, w(j-1)+w1)
                } else {
                    backtrack(i-1, j, v(i-1)+v1, "-"+w1)
                }
            }
        }

        val (v1, w1) = backtrack(v.length, w.length, "", "")
        List((mr, mc), previous)
    }

    def greedySort(data: Array[Int]): Array[Array[Int]] = {
        def revnsign(data: Array[Int], _from: Int, _to: Int): Array[Int] = {
            val count = (_to - _from)/2
            (0 to count).foreach { i =>
                val x = -data(_from + i)
                data(_from + i) = -data(_to - i)
                data(_to - i) = x
            }

            //println("from - " + _from + ", to = " + _to)

/*            if ((_to - _from)%2 == 0) {
               val i = _from + (_to - _from)/2
               data(i) = -data(i)
            }*/

            data
        }

        @tailrec
        def greedySort0(i: Int, data: Array[Int], acc: List[Array[Int]]): List[Array[Int]] = if (i != data.length) {
            if (data(i) == i+1) {
                greedySort0(i+1, data, acc)
            } else if (Math.abs(data(i)) == i+1) {
                data(i) = -data(i)
                greedySort0(i+1, data, acc :+ data.clone)
            } else {
                val j = data.indexWhere(Math.abs(_) == i+1, i)
                val newdata = revnsign(data, i, j)
                greedySort0(i, newdata, acc :+ newdata.clone)
            }
        } else acc

        greedySort0(0, data, List()).toArray
    }

    def numberOfBreakpoints(data: Array[Int]): Int = {
        @tailrec
        def nob(i: Int, acc: Int): Int = if (i != data.length) {
            if (data(i) - data(i-1) != 1)
                nob(i+1, acc+1)
            else
                nob(i+1, acc)
        } else acc

        nob(1, 0)
    }

    def sharedKMers(k: Int, v: String, w: String): List[(Int, Int)] = {
        val wKMers = w.kmers(k)

        v.kmers(k).foldLeft(List[(Int, Int)]()) { (acc, pair) =>
            val kmer = pair._1
            val shared = if (wKMers.contains(kmer)) { pair._2.flatten { i => wKMers(kmer).map((i, _))} } else Nil
            val revkmer = kmer.reverseComplement
            val revshared = if (wKMers.contains(revkmer)) { pair._2.flatten { i => wKMers(revkmer).map((i, _))} } else Nil
            acc ++ shared ++ revshared
        }.sortBy(x => (x._2, x._1))
    }

    def makeGraph(redEdges: List[Edge], blueEdges: List[Edge]) = (redEdges ++ blueEdges).foldLeft(Map[Vertex, List[Edge]]()) { (graph, edge) =>
        val start = if (graph.contains(edge.start)) graph(edge.start) :+ edge else List(edge)
        val end = if (graph.contains(edge.end)) graph(edge.end) :+ edge else List(edge)

        graph + (edge.start -> start) + (edge.end -> end)
    }

    def buildGraph(redItems: List[List[Int]], blueItems: List[List[Int]]) = {
        def makeColoredEdges(c: Color, blackEdges: List[Edge], items_items: List[List[Int]]) = {
            def connect(c: Color, prev: Edge, curr: Edge, prevValue: Int, currValue: Int) = {
                val start = if (prevValue > 0) prev.end else prev.start
                val end = if (currValue > 0) curr.start else curr.end
                Edge(c, start, end)
            }
            def find(value: Int, black: List[Edge]) = black.find(_.value == Math.abs(value))

            items_items.foldLeft(List[Edge]()) { (coloredEdges, items) =>
                val first = items.head
                val (last, newColoredEdges) = items.tail.foldLeft((first, List[Edge]())) { case ((prev, red), curr) =>
                    (curr, red :+ connect(c, find(prev, blackEdges).get, find(curr, blackEdges).get, prev, curr))
                }

                coloredEdges ++ newColoredEdges :+ connect(c, find(last, blackEdges).get, find(first, blackEdges).get, last, first)
            }
        }

        val blackEdges = redItems.foldLeft(List[Edge]()) { (be, items) => be ++ items.map( item => Edge(BLACK, Vertex(), Vertex(), Math.abs(item))) }
        val redEdges = makeColoredEdges(RED, blackEdges, redItems)
        val blueEdges = makeColoredEdges(BLUE, blackEdges, blueItems)

        (makeGraph(redEdges, blueEdges), redEdges, blueEdges, blackEdges)
    }

    def cycles(graph: Map[Vertex, List[Edge]], coloredEdges: List[Edge]): List[List[Edge]] = {
        def cycles0(acc: List[List[Edge]]): List[List[Edge]] = {
            def dfs(init: Vertex): List[Edge] = {
                var v = init
                var edgeOption = graph(v).find(_.visited == 0)
                val acc = mutable.ListBuffer[Edge]()
                while (edgeOption.isDefined) {
                    val e = edgeOption.get
                    acc += e

                    e.visited = 1
                    v = if (e.start == v) e.end else e.start
                    edgeOption = graph(v).find(_.visited == 0)
                }
                acc.toList
            }

            coloredEdges.find(_.visited == 0) match {
                case Some(edge) => {
                    val cycle = dfs(edge.start)
                    cycles0(acc :+ cycle)
                }
                case None => acc
            }
        }

        cycles0(List())
    }

    def twoBreakDistance(redItems: List[List[Int]], blueItems: List[List[Int]]): Int = {
        val (graph, redEdges, blueEdges, blackEdges) = buildGraph(redItems, blueItems)
        val coloredEdges = redEdges ++ blueEdges

        def blocks: Int = redItems.flatten.length

        blocks - cycles(graph, coloredEdges).length
    }

    def twoBreakSort(d1: List[Int], d2: List[Int]): List[List[List[Int]]] = {
        def nonTrivial(cycles: List[List[Edge]]): Option[List[Edge]] = {
            println(cycles)
            cycles.find { cycle =>
                cycle.length > 2 && cycle.find(_.color == RED).isDefined && cycle.find(_.color == BLUE).isDefined
            }
        }

        def restore(redEdges: List[Edge], blackEdges: List[Edge]): List[List[Int]] = {
            List(List(0))
        }

        def twoBreakSort0(redEdges: List[Edge], blueEdges: List[Edge],
                          blackEdges: List[Edge], acc: List[List[List[Int]]]): List[List[List[Int]]] = {
            nonTrivial(cycles(makeGraph(redEdges, blueEdges), redEdges ++ blueEdges)) match {
                case Some(c) => {
                    val be = c.find(_.color == BLUE).get

                    val re1 = c.find(e => e.color == RED && e.end == be.start).get
                    val re2 = c.find(e => e.color == RED && e.start == be.end).get

                    val newRedEdges = redEdges.filter(e => e != re1 && e != re2) :+ Edge(RED, re1.end, re2.start) :+ Edge(RED, re2.end, re1.start)

                    val newP = restore(newRedEdges, blackEdges)
                    twoBreakSort0(newRedEdges, blueEdges, blackEdges, acc :+ newP)
                }
                case None => acc
            }
        }


        val (graph, redEdges, blueEdges, blackEdges) = buildGraph(List(d1), List(d2))
        twoBreakSort0(redEdges, blueEdges, blackEdges, List(List(d1)))
    }
}

sealed case class TreeWithError(var height: Int, var errorCount: Int, var tree: Tree[Char, Int])

sealed case class Tree[T, V](parent: Tree[T, V], children: mutable.Map[T, Tree[T, V]], var k: T, var v: V) {
    override def equals(obj: scala.Any): Boolean = false
    override def hashCode(): Int = k.hashCode
}

object IDGenerator {
    var id = 0
    def nextID = { id += 1; id }
}

sealed case class Vertex(id: Int = IDGenerator.nextID) { override def toString: String = "Vertex(" + id + ")" }
sealed case class Edge(color: Color, var start: Vertex, var end: Vertex, value: Int = -1, var visited: Int = 0)


