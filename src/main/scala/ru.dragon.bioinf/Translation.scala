package ru.dragon.bioinf

import scala.annotation.tailrec
import scala.collection.immutable.TreeMap
import scala.collection._

/**
 * @author NIzhikov
 */
trait Translation {
    def DNA_ALFABET = List("G", "C", "A", "T")

    def RNA_2_PEPTID = Map(
            "AAA" -> "K", "AAC" -> "N", "AAG" -> "K",
            "AAU" -> "N", "ACA" -> "T", "ACC" -> "T",
            "ACG" -> "T", "ACU" -> "T", "AGA" -> "R",
            "AGC" -> "S", "AGG" -> "R", "AGU" -> "S",
            "AUA" -> "I", "AUC" -> "I", "AUG" -> "M",
            "AUU" -> "I", "CAA" -> "Q", "CAC" -> "H",
            "CAG" -> "Q", "CAU" -> "H", "CCA" -> "P",
            "CCC" -> "P", "CCG" -> "P", "CCU" -> "P",
            "CGA" -> "R", "CGC" -> "R", "CGG" -> "R",
            "CGU" -> "R", "CUA" -> "L", "CUC" -> "L",
            "CUG" -> "L", "CUU" -> "L", "GAA" -> "E",
            "GAC" -> "D", "GAG" -> "E", "GAU" -> "D",
            "GCA" -> "A", "GCC" -> "A", "GCG" -> "A",
            "GCU" -> "A", "GGA" -> "G", "GGC" -> "G",
            "GGG" -> "G", "GGU" -> "G", "GUA" -> "V",
            "GUC" -> "V", "GUG" -> "V", "GUU" -> "V",
            "UAA" -> "", "UAC" -> "Y", "UAG" -> "",
            "UAU" -> "Y", "UCA" -> "S", "UCC" -> "S",
            "UCG" -> "S", "UCU" -> "S", "UGA" -> "",
            "UGC" -> "C", "UGG" -> "W", "UGU" -> "C",
            "UUA" -> "L", "UUC" -> "F", "UUG" -> "L",
            "UUU" -> "F"
    )

    def PEPTID_2_RNA = Map(
        'A' -> List("GCA", "GCC", "GCG", "GCU"),
        'C' -> List("UGC", "UGU"),
        'D' -> List("GAC", "GAU"),
        'E' -> List("GAA", "GAG"),
        'F' -> List("UUC", "UUU"),
        'G' -> List("GGA", "GGC", "GGG", "GGU"),
        'H' -> List("CAC", "CAU"),
        'I' -> List("AUA", "AUC", "AUU"),
        'K' -> List("AAA", "AAG"),
        'L' -> List("CUA", "CUC", "CUG", "CUU", "UUA", "UUG"),
        'M' -> List("AUG"),
        'N' -> List("AAC", "AAU"),
        'P' -> List("CCA", "CCC", "CCG", "CCU"),
        'Q' -> List("CAA", "CAG"),
        'R' -> List("AGA", "AGG", "CGA", "CGC", "CGG", "CGU"),
        'S' -> List("AGC", "AGU", "UCA", "UCC", "UCG", "UCU"),
        'T' -> List("ACA", "ACC", "ACG", "ACU"),
        'V' -> List("GUA", "GUC", "GUG", "GUU"),
        'W' -> List("UGG"),
        'Y' -> List("UAC", "UAU")
    )

    def peptidWeights = TreeMap(
        'G' -> 57, 'A' -> 71, 'S' -> 87,
        'P' -> 97, 'V' -> 99, 'T' -> 101,
        'C' -> 103, 'I' -> 113, 'L' -> 113,
        'N' -> 114, 'D' -> 115, 'K' -> 128,
        'Q' -> 128, 'E' -> 129, 'M' -> 131,
        'H' -> 137, 'F' -> 147, 'R' -> 156,
        'Y' -> 163, 'W' -> 186
    )

    def peptideMasses = peptidWeights.values.toSet

    implicit class BioInfCharTranslation(data: Char) {
        def peptideMass: Int = peptidWeights(data)
    }

    implicit class BioInfStringTranslation(data: String) {
        def reverseComplement: String =
            data.reverse.
                replace('A', 'X').
                replace('T', 'A').
                replace('X', 'T').
                replace('C', 'X').
                replace('G', 'C').
                replace('X', 'G')

        def rna2peptid: String = {
            def l = data.length
            @tailrec
            def r2p(i: Int, acc: String): String =
                if (l - i >= 2)
                    r2p(i+3, acc + RNA_2_PEPTID(data.substring(i, i+3)))
                else
                    acc

            r2p(0, "")
        }
        
        def peptid2rna: List[String] = {
            def l = data.length
            def p2r(i: Int, acc: List[String]): List[String] =
                if (i < l)
                    PEPTID_2_RNA(data(i)).flatMap { aminoDNA => p2r(i + 1, acc.map(_ + aminoDNA))}
                else
                    acc

            p2r(0, List(""))
        }

        def peptideMass: Int = {
            var pm = 0
            data.foreach{pm += peptidWeights(_)}
            pm
        }

        def expandPeptides: Set[String] = peptidWeights.keySet.map(e => data + e)

        def toDNA: String = data.replace('U', 'T')
        def toRNA: String = data.replace('T', 'U')
    }

    implicit class BioInfInt(n: Int) {
        def kmers: List[String] = {
            def kmers0(i: Int): List[String] = if (i == 1)
                DNA_ALFABET
            else
                DNA_ALFABET.flatMap { ch => kmers0(i-1).map(_ + ch) }

            if (n > 1)
                kmers0(n)
            else
                Nil
        }
    }

    def dpChange(money: Int, coins: List[Int]): List[Int] = {
        val answers = mutable.Map[Int, List[Int]]()

        def dpChange0(m: Int, coins: List[Int]): List[Int] = if (answers.contains(m)) {
            answers(m)
        } else {
            if (m != 0) {
                val coin = coins.head
                if (m >= coin) {
                    val rest = dpChange0(m-coin, coins) :+ coin
                    if (coins.tail.isEmpty) {
                        answers += m -> rest
                        rest
                    } else {
                        val rest2 = dpChange0(m, coins.tail)
                        if (rest.length < rest2.length) {
                            answers += m -> rest
                            rest
                        } else {
                            answers += m -> rest2
                            rest2
                        }
                    }
                } else  if (!coins.tail.isEmpty) {
                    val answer = dpChange(m, coins.tail)
                    answers += m -> answer
                    answer
                } else
                    throw new RuntimeException("!!!!")
            } else {
                answers += m -> List()
                List()
            }
        }

        dpChange0(money, coins)
    }
}
