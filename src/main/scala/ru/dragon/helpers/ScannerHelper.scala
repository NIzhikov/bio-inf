package ru.dragon.helpers

import java.util.Scanner

import scala.annotation.tailrec

/**
 * @author NIzhikov
 */
trait ScannerHelper {
    implicit class ScannerExt(sc: Scanner) {
        def getWhile(predicate: String => Boolean): List[String] = {
            @tailrec
            def getWhile0(acc: List[String]): List[String] =
                if (sc.hasNext) {
                    val line = sc.nextLine
                    if (predicate(line))
                        getWhile0(acc :+ line)
                    else
                        acc :+ line
                } else acc

            getWhile0(List())
        }

        def all: List[String] = getWhile(_ => true)
    }

}
