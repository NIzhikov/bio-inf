package ru.dragon.helpers

/**
 * @author NIzhikov
 */
object EdgeColors extends Enumeration {
    type Color = Value
    val BLUE, RED, BLACK = Value
}
