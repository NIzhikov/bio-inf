package ru.dragon.bioinf

import java.util.Scanner

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import ru.dragon.helpers.ScannerHelper

import scala.annotation.tailrec
import scala.collection._
import scala.collection.SortedSet
import scala.io.Source.fromURL

/**
 * @author NIzhikov
 */
@RunWith(classOf[JUnitRunner])
class NewSuite extends FunSuite with BioInfAlgorithmes with ScannerHelper {
    test("Reconstruction") {
        assert("CAATCCAAC".substrSet(5) === SortedSet("AATCC", "ATCCA", "CAATC", "CCAAC", "TCCAA"))
    }

    test("Reconstruction - Extra") {
        val sc = new Scanner(fromURL(getClass.getResource("/string_com.txt")).bufferedReader)
        val l = sc.nextInt
        sc.nextLine
        val data = sc.nextLine

        @tailrec
        def getWhile(sc: Scanner, acc: SortedSet[String]): SortedSet[String] =
            if (sc.hasNext) getWhile(sc, acc + sc.nextLine) else acc
        val answer = getWhile(sc, SortedSet[String]())

        assert(data.substrSet(l) === answer)
    }

    test("String Spelled by a Genome Path") {
        val data = List("ACCGA", "CCGAA", "CGAAG", "GAAGC", "AAGCT")
        assert(data.reconstruct === "ACCGAAGCT")
    }

    test("String Spelled by a Genome Path - Extra") {
        val sc = new Scanner(fromURL(getClass.getResource("/GenomePathString.txt")).bufferedReader)

        val allData = sc.all

        val answer = allData.last
        val data = allData.dropRight(1)

        assert(data.reconstruct === answer)
    }

    test("Overlap Graph Problem") {
        val data = List("ATGCG", "GCATG", "CATGC", "AGGCA", "GGCAT")

        val answer = data.overlapGraph
        assert(answer === Map("AGGCA" -> "GGCAT", "CATGC" -> "ATGCG", "GCATG" -> "CATGC", "GGCAT" -> "GCATG"))
    }

    test("Overlap Graph Problem - Extra") {
        val sc = new Scanner(fromURL(getClass.getResource("/overlap_graph_1.txt")).bufferedReader)

        sc.nextLine
        val data = sc.getWhile(_ != "Output:").dropRight(1)

        val itemLength = data.head.length
        val output: Map[String, String] = sc.all.map(v => {
            (v.substring(0, itemLength), v.substring(itemLength + " -> ".length))
        }).toMap

        assert(data.overlapGraph === output)
    }

    test("De Bruijn Graph from a String") {
        val answer = "AAGATTCTCTAAGA".deBrujinGraph(4)
        assert(answer === Map("AAG" -> List("AGA","AGA"), "AGA" -> List("GAT"), "ATT" -> List("TTC"),
            "CTA" -> List("TAA"), "CTC" -> List("TCT"), "GAT" -> List("ATT"),
            "TAA" -> List("AAG"), "TCT" -> List("CTA","CTC"), "TTC" -> List("TCT")))
    }

    test("De Bruijn Graph from a String - Extra") {
        val sc = new Scanner(fromURL(getClass.getResource("/De_Bruijn_Graph_from_a_String.txt")).bufferedReader)

        sc.nextLine
        val k = sc.nextInt
        sc.nextLine
        val data = sc.nextLine
        sc.nextLine
        val rightAnswer = sc.all.map { line =>
            val first::second::tail = line.split(" -> ").toList
            (first, second.split(",").toList)
        }.toMap

        assert(data.deBrujinGraph(k) === rightAnswer)
    }

    test("De Bruijn Graph from a list") {
        val answer = List("GAGG", "CAGG", "GGGG", "GGGA",
            "CAGG", "AGGG", "GGAG").deBrujinGraph
        assert(answer === Map("AGG" -> List("GGG"),
            "CAG" -> List("AGG", "AGG"),
            "GAG" -> List("AGG"),
            "GGA" -> List("GAG"),
            "GGG" -> List("GGA", "GGG")))
    }

    test ("Eulerian path") {
        val data = Map[Int, List[Int]](0 -> List(2), 1 -> List(3), 2 -> List(1), 3 -> List(0,4),
        6 -> List(3,7), 7 -> List(8), 8 -> List(9), 9 -> List(6))

        assert(eulerianPath(data) === List(6, 7, 8, 9, 6, 3, 0, 2, 1, 3, 4))
    }

    test("From Euler's Theorem to an Algorithm for Finding Eulerian Cycles - 3") {
        val sc = new Scanner(fromURL(getClass.getResource("/dataset_203_6.txt")).bufferedReader)
        val k = sc.nextLine.toInt
        assert(eulerianPath(sc.all.deBrujinGraph).reconstruct === "ACAGTGGACGAGACCGTGCCCGGACTGCAGACTCGTCACGGTTCAAAATTCCATCTTAAAAGGGATCAATCATACGGGGGCAAAGGCCAAACGCATGGGACGGAGAAACCTGCATTCAAGTTGCTGTATTCTAGACCCCGTAGAGGAGTTTAAGCTCAGGACCGAAGAGATCGGTTGTTCCTGATGACTCATAGAGCAACTTCATCTCGCGAGAAGACACTAGAGGCCCGCTGATCGCCCGACCTGCACAATGGCAACATCCTCCGTGTGGAAAGCGCGGGCTGGTCAACGACTTCAGGAGATTGGCGATGGTCGGTAGACTTACTAGTTCCTTTCCGTAGATTTACATATGGCTGGTGCTCCATCGTCTCCGATTCTGAGCTTCGGTCAGAGCGGGTGTAAGCTCTGAACGGTATAATAACCTCTGCATCAGGTCCCTATGGCAAACAGTTGAACGTATCCTTTAATAGGAGGTTCGCAGCTAGGGTAAAACAACTTTTAGTGTTACGTTACACGCCTGCAAAGCCTCTTATCTCTCATGAAGTATATCCCAGTTCGGTGGAGGACGGTATAGTAAACCTGTCTCCATTGGGGTGTACGGCGCGCGCCTTCGGCACTGGGTTCGATAGCTTACGTGAATGCTTTTATAACGTATCGAGGGGCCAGAATCACGCTGGTTATACGGGCTTGGTGTGGATCTCCAACCGAGATGAATCAACAGCGGCGTGAAGGATGAGTAATGGTACCAAGTTGTGACAGTCTAAGCAGATTCCGGTTAGATTCTTCACCGTTACGATGGCTCCTCAGGCTAGTGGGGGCGATTGGTTTCACCCGTCTGATCCAGAGCCAGCGTACAACGAGAGACGTGCAATCTACTGATTATACGACAAGCCCCCGAGATCGGGGAATGGCTTGGCTTTTAGAAACTATATAGGAGCCTCACTGTGCATCGTTCTCTATCCTAGATAAAACTTTATGCCTGTCAGGTTCTTCACACTAGCCTGGACTAGTGTTTCGTTACGATGACCGTGCCACTTTATAGGTTTCTGAGCAGGGGGCGTGATACGTATCTGATGGTTAGGTACACCGGGATTACGAAGTGACTTTTGGATGCCGGAGGGGCTAACACCGTTCACTAAGAAGCTCTTACCTATCGGTCCGGCAGCGGGCAAACACGACGAGTATAGAAATGTACCCTCCTGTTAAATAAACGCGCGTAGAGTATTTCTGGATCGATCTGAACATCGGGCATGATTATCCACCAAAAATATTCCATGAGGGGCTACAATTATTACCTCACTCGCATGTTTTAGGTACTGTGGTTATATTCTCGCGATGACCCCAGTGTTAGTTACTGTATCTCAATACTCACTTAGGGGGTACGAAATTAACTTGTCTGTGGTACCTACACGTTTGCAGTCAAGAGGATAACAACCCCCAGTATTCTTGACGATCCAAACGCGTAATCGTATTCCAGCGACATCTCCGCGACCCGCATGTTAGATGACCAGATCACACATCGCCGCGTTCACTAAACAGGCTCCATGTTTTCAACATAATTGCCTTTGGCGGAGCAAGGCTATCCCCACATAGCAGCCACCCACGTACTACTTTGCTCCTGAAAGCAATGCGGGTTGATGCGAACTTAGTGACTCGCGGATATTCCTCTTGCATCGATGACGCAATCTGGCCACACGCTGCCATACGGAAGTTCTTTCTTCACGATCTCAAGTCCCGCGTTAGGTAAGGATCGGCGACCTGCCTGAGAATGACCTTATAAGCAACAACCAATGATAGAGGCGTGGCCACGATGAGCCCCGTCGGAAGACAAGGCACGACTGGCATGAAATAATCGGGAAGGATGCGGTAAGCAAGGTAGACGAACGTGCGCAGGCTGAGGATGGAACGGCACCGGAGTGCCTCGTCCGAGTATACACAGTACGTACCGTTGCCTATAATCAAAGCAAAGAGGTCTTGATCGGCTAGGTGTTAAGTGACTTGGTGGGCCTTTGAGAATTGGGATAGGGTATTCAGTGGCCAAACCTCTGCCGTTCAGATTTGATCGTGCTATTTTTCGCCGGCGGGGGAGCTTAGCACTTACCGACAGCAGAAAGCCGCAGTACGGTGATTCCTCTCCTGAGAATCGCGAAGCGGGGCTTGTAGCTTTGTATGCCATCGGCCTGGTGTTTAAGGCACAATCCGCCAAGAACTTGCCGCAGGATGACCACCCTGAATTCCTG")
    }

    test("From Euler's Theorem to an Algorithm for Finding Eulerian Cycles") {
        val data = List("CTTA", "ACCA", "TACC", "GGCT", "GCTT", "TTAC")
        assert(eulerianPath(data.deBrujinGraph).reconstruct === "GGCTTACCA")
    }
}
