package ru.dragon.bioinf

import java.io.{FileWriter, FileOutputStream}

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import java.util.Scanner
import ru.dragon.helpers.ScannerHelper
import scala.collection._

import scala.io.Source.fromURL

/**
 * @author NIzhikov
 */
@RunWith(classOf[JUnitRunner])
class Week6Suite extends FunSuite with BioInfAlgorithmes with ScannerHelper {
    test("GreedySorting") {
        val answer = greedySort(Array(-3, +4, +1, +5, -2))
        val rightAnswer = Array(
            Array(-1, -4, +3, +5, -2),
            Array(+1, -4, +3, +5, -2),
            Array(+1, +2, -5, -3, +4),
            Array(+1, +2, +3, +5, +4),
            Array(+1, +2, +3, -4, -5),
            Array(+1, +2, +3, +4, -5),
            Array(+1, +2, +3, +4, +5)
        )

        (0 to rightAnswer.length - 1).foreach { i =>
            assert(answer(i) === rightAnswer(i))
        }
    }

    test("GreedySorting - Extra") {
        val sc = new Scanner(fromURL(getClass.getResource("/greedy_sorting.txt")).bufferedReader)
        sc.nextLine

        val input = sc.nextLine.replace("(", "").replace(")", "").split(" ").map(_.toInt).toArray
        val answer = greedySort(input)

        sc.nextLine
        val rightAnswer = sc.all.map {
            _.replace("(", "").replace(")", "").split(" ").map(_.toInt)
        }

        (0 to rightAnswer.length - 1).foreach { i =>
            assert(answer(i) === rightAnswer(i))
        }
    }

    test("GreedySorting - 3") {
        val sc = new Scanner(fromURL(getClass.getResource("/dataset_286_3.txt")).bufferedReader)
        val input = sc.nextLine.replace("(", "").replace(")", "").split(" ").map(_.toInt).toArray
        val answer = greedySort(input)

        val fw = new FileWriter("/home/dragon/tmp/output.txt")

        (0 to answer.length - 1).foreach { i =>
            fw.write("(")
            val y = answer(i).head
            if (y > 0) fw.write("+" + y) else fw.write("" + y)
            answer(i).tail.foreach(x => if (x > 0) fw.write(" +" + x) else fw.write(" " + x))
            fw.write(")\n")
        }

        fw.close
    }

    test("NumberOfBreakpoints") {
        val data = Array(+3, +4, +5, -12, -8, -7, -6, +1, +2, +10, +9, -11, +13, +14)

        assert(numberOfBreakpoints(data) === 7)
    }

    test("NumberOfBreakpoints - Extra") {
        val sc = new Scanner(fromURL(getClass.getResource("/number_of_breaks.txt")).bufferedReader)
        sc.nextLine

        val input = sc.nextLine.replace("(", "").replace(")", "").split(" ").map(_.toInt).toArray
        val answer = numberOfBreakpoints(input)

        sc.nextLine
        val rightAnswer = sc.nextLine.toInt

        assert(answer === rightAnswer)
    }

    test("2-break distance") {
        val data1 = List(List(+1, +2, +3, +4, +5, +6))
        val data2 = List(List(+1, -3, -6, -5), List(+2, -4))

        val answer = twoBreakDistance(data1, data2)
        assert(answer === 3)
    }

    test("2-break distance - Extra") {
        val sc = new Scanner(fromURL(getClass.getResource("/2_break.txt")).bufferedReader)
        sc.nextLine

        val data1 = sc.nextLine.split("\\)\\(").map { line =>
            line.replace("(", "").replace(")", "").split(" ").map(_.toInt).toList
        }.toList
        val data2 = sc.nextLine.split("\\)\\(").map { line =>
            line.replace("(", "").replace(")", "").split(" ").map(_.toInt).toList
        }.toList
        sc.nextLine()

        val answer = twoBreakDistance(data1, data2)

        val rightAnswer = sc.nextLine.toInt
        assert(answer === rightAnswer)
    }

    test("2-break distance - 3") {
        val sc = new Scanner(fromURL(getClass.getResource("/dataset_288_4.txt")).bufferedReader)

        val data1 = sc.nextLine.split("\\)\\(").map { line =>
            line.replace("(", "").replace(")", "").split(" ").map(_.toInt).toList
        }.toList
        val data2 = sc.nextLine.split("\\)\\(").map { line =>
            line.replace("(", "").replace(")", "").split(" ").map(_.toInt).toList
        }.toList

        val answer = twoBreakDistance(data1, data2)
        assert(answer === 9059)
    }

    test("2-break sorting") {
        val data1 = List(+1, -2, -3, +4)
        val data2 = List(+1, +2, -4, -3)

        val answer = twoBreakSort(data1, data2)

        assert(answer === List(
            List(List(+1, -2, -3, +4)),
            List(List(+1, -2, -3), List(+4)),
            List(List(+1, -2, -4, -3)),
            List(List(-3, +1, +2, -4))
        ))
    }

    test("2-break sorting - Extra") {
        val sc = new Scanner(fromURL(getClass.getResource("/2BreakSorting.txt")).bufferedReader)
        sc.nextLine

        val data1 = sc.nextLine().replace("(", "").replace(")", "").split(" ").map(_.toInt).toList
        val data2 = sc.nextLine().replace("(", "").replace(")", "").split(" ").map(_.toInt).toList

        sc.nextLine
        val rightAnswer = sc.all.map { line =>
            line.split("\\)\\(").map { line =>
                line.replace("(", "").replace(")", "").split(" ").map(_.toInt).toList
            }.toList
        }

        val answer = twoBreakSort(data1, data2)
        assert(answer === rightAnswer)
    }
}
