package ru.dragon.bioinf

import org.junit.runner.RunWith
import java.util.Scanner
import scala.io.Source.fromURL
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import ru.dragon.helpers.ScannerHelper
import scala.collection._

/**
 * @author NIzhikov
 */
@RunWith(classOf[JUnitRunner])
class Week5Suite extends FunSuite with BioInfAlgorithmes with ScannerHelper {
    test("DpChange") {
        assert(dpChange(40, List(50, 25, 20, 10, 5, 1)) === List(20, 20))
    }

    test("Length of a longest path in the Manhattan Tourist") {
        val down = Array(
            Array(1, 0, 2, 4, 3),
            Array(4, 6, 5, 2, 1),
            Array(4, 4, 5, 2, 1),
            Array(5, 6, 8, 5, 3)
        )

        val right = Array(
            Array(3, 2, 4, 0),
            Array(3, 2, 4, 2),
            Array(0, 7, 3, 3),
            Array(3, 3, 0, 2),
            Array(1, 3, 2, 2)
        )

        assert(manhattanTouristPath(4, 4, down, right) === 34)
    }

    test("Length of a longest path in the Manhattan Tourist - Extra") {
        val sc = new Scanner(fromURL(getClass.getResource("/longest_path_1.txt")).bufferedReader)
        sc.nextLine()

        val n = sc.nextInt
        val m = sc.nextInt
        sc.nextLine

        val down = sc.getWhile(_ != "-").dropRight(1).map(_.split(" ").map(_.toInt)).toArray
        val right = sc.getWhile(_ != "").dropRight(1).map(_.split(" ").map(_.toInt)).toArray

        assert(manhattanTouristPath(m, n, down, right) === 84)
    }

    test("Length of a longest path in the Manhattan Tourist - 3") {
        val sc = new Scanner(fromURL(getClass.getResource("/dataset_261_9.txt")).bufferedReader)

        val n = sc.nextInt
        val m = sc.nextInt
        sc.nextLine

        val down = sc.getWhile(_ != "-").dropRight(1).map(_.split(" ").map(_.toInt)).toArray
        val right = sc.all.map(_.split(" ").map(_.toInt)).toArray

        assert(manhattanTouristPath(m, n, down, right) === 58)
    }

    test("Longest Common SubSequence - Extra") {
        val sc = new Scanner(fromURL(getClass.getResource("/longest_common_subsequence.txt")).bufferedReader)
        sc.nextLine
        val v = sc.nextLine
        val w = sc.nextLine
        sc.nextLine

        assert(outputLCS(v, w) === sc.nextLine)
    }

    test ("Longest Path in a DAG") {
        val start = 0
        val end = 4
        val (weight, path) = findLongestPath(start, end, Map(
            0->List((1,7), (2,4)),
            2->List((3,2)),
            1->List((4,1)),
            3->List((4,3)))
        )

        assert(weight === 9)
        assert(path === List(0,2,3,4))
    }

    test ("Longest Path in a DAG - Extra") {
        val sc = new Scanner(fromURL(getClass.getResource("/longest_path_in_DAG.txt")).bufferedReader)
        sc.nextLine
        val start = sc.nextLine.toInt
        val end = sc.nextLine.toInt

        val graph = mutable.Map[Int, List[(Int, Int)]]()
        sc.getWhile(_!="").dropRight(1).map { line =>
            val arr = line.split("->")
            val tuple = arr(1).split(":")

            graph += arr(0).toInt -> (graph.getOrElse(arr(0).toInt, List[(Int, Int)]()) :+ (tuple(0).toInt -> tuple(1).toInt))
        }

        sc.nextLine

        val rightWeight = sc.nextLine.toInt
        val rightPath = sc.nextLine.split("->").map(_.toInt).toList

        val (weight, path) = findLongestPath(start, end, graph)

        assert(weight === rightWeight)
        assert(path === rightPath)
    }

    test ("Longest Path in a DAG - 3") {
        val sc = new Scanner(fromURL(getClass.getResource("/dataset_245_7.txt")).bufferedReader)
        val start = sc.nextLine.toInt
        val end = sc.nextLine.toInt

        val graph = mutable.Map[Int, List[(Int, Int)]]()
        sc.all.map { line =>
            val arr = line.split("->")
            val tuple = arr(1).split(":")

            graph += arr(0).toInt -> (graph.getOrElse(arr(0).toInt, List[(Int, Int)]()) :+ (tuple(0).toInt -> tuple(1).toInt))
        }

        val (weight, path) = findLongestPath(start, end, graph)

        assert(weight === 42)
        assert(path === List(9, 19, 28))
    }

    test ("Local Alignment") {
        val (score, v, w) = localAlignment("MEANLY", "PENALTY", readMatrix("/PAM250_1.txt"), 5)

        assert(score === 15)
        assert(v === "EANL-Y")
        assert(w === "ENALTY")
    }

    test ("Local Alignment - Extra") {
        val sc = new Scanner(fromURL(getClass.getResource("/local_alignment.txt")).bufferedReader)

        sc.nextLine
        val (score, v, w) = localAlignment(sc.nextLine, sc.nextLine, readMatrix("/PAM250_1.txt"), 5)
        sc.nextLine

        assert(score === sc.nextLine.toInt)
        assert(v === sc.nextLine)
        assert(w === sc.nextLine)
    }

    test ("Local Alignment - Extra - 2") {
        val sc = new Scanner(fromURL(getClass.getResource("/local_alignment.txt")).bufferedReader)

        val (score, v, w) = localAlignment("AMTAFRYRQGNPRYVKHFAYEIRLSHIWLLTQMPWEFVMGIKMPEDVFQHWRVYSVCTAEPMRSDETYEQKPKPMAKWSGMTIMYQAGIIRQPPRGDRGVSDRNYSQCGKQNQAQLDNNPTWTKYEIEWRVQILPPGAGVFEGDNGQNQCLCPNWAWEQPCQWGALHSNE",
            "QVCEYSCNSHSCGWMPIFCIVCMSYVAFYCGLEYPMSRKTAKSQFIEWCDWFCFNHWTNWAPLSIVRTSVAFAVWGHCWYPCGGVCKTNRCKDDFCGRWRKALFAEGPRDWKCCKNDLQNWNPQYSQGTRNTKRMVATTNQTMIEWKQSHIFETWLFCHVIIEYNWSAFWMWMNRNEAFNS", readMatrix("/PAM250_1.txt"), 5)

        assert(score === 109)
        assert(v === "AFRYRQGNPRYVKHFAYEIRLSHIWL-LTQ-MPWEFVMGIKMPEDV-FQHW-RV-YSVCTAEPMRSDETYEQKPKPMAKWSGMTIMYQAGII-RQPPRGDRGVSDRNYSQCGKQNQAQLDNNPTWTKYEIEWR-VQILPPGAGVF-EGDNGQNQCLCPNWAW")
        assert(w === "GLEYPMSR-KTAKS-QF-IEWCD-WFCFNHWTNWA-PLSI-VRTSVAFAVWGHCWYP-CGGVC-KTNR-CKDDF-C-GRWR-KA-LFAEGPRDWKCCKNDLQNWNPQYSQ-GTRNTKRMVAT-T-NQTMIEWKQSHIF--ETWLFCHVII-EYN-WSAFWMW")
    }

    test ("Local Alignment - Extra - 3") {
        val sc = new Scanner(fromURL(getClass.getResource("/local_alignment.txt")).bufferedReader)

        val (score, v, w) = localAlignment("RQPPRGDRGVSDRNYSQCGKQNQAQLDNNPTWTKYEIEWRVQILPPGAGVFEGDNGQNQCLCPNWAWEQPCQWGALHSNEQYPNRIHLWAPMSKLHIKIEKSSYNRNAQFPNRCMYECEFPSYREQVDSCHY",
            "FYCGLEYPMSRKTAKSQFIEWCDWFCFNHWTNWAPLSIVRTSVAFAVWGHCWYPCGGVCKTNRCKDDFCGRWRKALFAEGPRDWKCCKNDLQNWNPQYSQGTRNTKRMVATTNQTMIEWKQSHIFET", readMatrix("/PAM250_1.txt"), 5)

        assert(score === 84)
        assert(v === "NYSQCGKQNQAQLDNNPTWTKYE--IEW-RVQILPPGAGVFEG-DNGQNQC--LC-PNWAWEQPC-QW-GALHSNEQYPNRIHLWAPMSKLHIKIE-KSSYNRNAQFPNRCM")
        assert(w === "EYPMSRKTAKSQFIEWCDWFCFNHWTNWAPLSIVRTSVA-FAVWGHCWYPCGGVCKTNRCKDDFCGRWRKALFA-EG-P-R-D-WKC-CKNDLQ-NWNPQYSQGTRNTKR-M")
    }

    test ("Edit Distance") {
        assert("PLEASANTLY".editDistance("MEANLY") === 5)
    }

    test ("Edit Distance - Extra") {
        val x = "GGACRNQMSEVNMWGCWWASVWVSWCEYIMPSGWRRMKDRHMWHWSVHQQSSPCAKSICFHETKNQWNQDACGPKVTQHECMRRRLVIAVKEEKSRETKMLDLRHRMSGRMNEHNVTLRKSPCVKRIMERTTYHRFMCLFEVVPAKRQAYNSCDTYTMMACVAFAFVNEADWWKCNCAFATVPYYFDDSCRMVCGARQCYRLWQWEVNTENYVSIEHAEENPFSKLKQQWCYIPMYANFAWSANHMFWAYIANELQLDWQHPNAHPIKWLQNFLMRPYHPNCGLQHKERITPLHKSFYGMFTQHHLFCKELDWRIMAHANRYYCIQHGWHTNNPMDPIDTRHCCMIQGIPKRDHHCAWSTCDVAPLQGNWMLMHHCHHWNRVESMIQNQHEVAAGIKYWRLNRNGKLPVHTADNYGVLFQRWWFLGWYNFMMWHYSLHFFAVNFYFPELNAGQMPRFQDDQNRDDVYDTCIWYFAWSNTEFMEVFGNMMMYSRPMTKMGFHGMMLPYIAINGLRSISHVNKGIGPISGENCNLSTGLHHYGQLRMVMCGYCTPYRTEVKNQREMISAVHCHQHIDWRWIWCSGHWFGSNKCDLRIEDLQNYEPAKNKSNWPYMKECRKTEPYQDNIETMFFHQHDLARDSGYIANGWHENCRQHQDFSNTFAGGHKGTPKGEHMRRSLYVWDTDCVEKCQWVPELFALCWWTPLPDGVPVMLGTYRQYMFGLVVLYWFEVKYSCHNSWDYYNFHEGTMKDSDPENWCFWGMQIIQFHDHGKPEFFQDPMKQIIKTECTAYNSFMMGHIGKTTIVYLVSYIGRLWMKSCCLTWPPYATAPIKWAEETLLDFGQGPHPKYACHFTHQNMIRLAKLPMYWLWKLMFHE"
        val y = "GMWGFVQVSTQSRFRHMWHWSVHQQSSECAKSICHHEWKNQWNQDACGPKVTQHECMANMPMHKCNNWFWRLVIAVKEEKVRETKMLDLIHRHWLVLNQGRMNEHNVTLRKSPCVKRIMHKWKSRTTFHRFMCLMASEVVPAKRGAQCWRQLGTYATYTVYTMMACVAFAFEYQQDNDNEADWWKCNCAFVPVYFDDSCRPVVGAFQCYRLGLPFGTGWNYAEENPFSKLKQQMHRKTMGECKNMMIWAYCANELQLPIKWGSMYHEHDFQLPPYHPNRFHKIRITILHKSFYGMFTQHHLFCKELDWRIMAWANRYYCIQHGWHTNNPDDPITRHKCMIQGGQNSRNADIRHMPVQCGNWGHAIGLEMPMPMHHCHHANRVESMIQTQHYWGPKLNRNADWWFLGWQNFEIFRMPILRWMGAYEWHYSLHFFAVNFYFPELNAGQMPRFQDDQNNNACYDVWAWSNTEFMEVNGIKKLRFGNMMMYSRPMTKMGFHGMMKSRSISHVNKGIGPISGENCSTGLHHYGQLTEVKNQREMISAVHCHQHIWCKCDLRIEPAKNKGYWPYQKEFCWRKQINSRKTEPYQVAPVINIETMFFDFWYIANGMHENCRRTGHKPNPDCVEKCQWVPELFALCWWRAMPDGVPVMLGTMFGLVVYWFEVKYSCHNSLYRRVTDYYNFHEGTMKDHEVPWNWDNEHCHDHGKAEFFFQMLKIPICDPMKAIIPSTEMVNTPWHPFSFMMGHDGKTTIVYSGSYIGRLWVPSRWKPYAPANWKMPIKWAEETLLMVPHPHFTHQQLWGTTLRLAKLPMYWLWKLMFHHLFGVK"
        assert(x.editDistance(y) === 400)
    }

    test ("Fitting Alignment - Extra") {
        val sc = new Scanner(fromURL(getClass.getResource("/fitting_alignment.txt")).bufferedReader)

        sc.nextLine
        val (score, v, w) = fittingAlignment(sc.nextLine, sc.nextLine, 1)
        sc.nextLine

        assert(score === sc.nextLine.toInt)
        assert(v === sc.nextLine)
        assert(w === sc.nextLine)
    }

    test ("Overlap Alignment") {
        val (score, v, w) = overlapAlignment("PAWHEAE", "HEAGAWGHEE", 2)

        assert(score === 1)
        assert(v === "HEAE")
        assert(w === "HEAG")
    }

    test("Alignment with Affine Gap Penalties Problem") {
        val (score, v, w) = affineGapPenaltiesAlignment("PRTEINS", "PRTWPSEIN", readMatrix("/BLOSUM62.txt"), 11, 1)

        assert(score === 8)
        assert(v === "PRT---EINS")
        assert(w === "PRTWPSEIN-")
    }

    test("Alignment with Affine Gap Penalties Problem - 2") {
        val (score, v, w) = affineGapPenaltiesAlignment("GCYTVVK", "DVVK", readMatrix("/BLOSUM62.txt"), 11, 1)

        assert(score === -1)
        assert(v === "GCYTVVK")
        assert(w === "---DVVK")
    }

    test ("Middle Edge ") {
        val answer = findMiddleEdge("PLEASANTLY", "MEASNLY", readMatrix("/BLOSUM62.txt"), 5)

        assert(answer === List((4,3), (5,4)))
    }

    test ("Middle Edge - Extra") {
        val x = "TWLNSACYGVNFRRLNPMNKTKWDCWTWVPMVMAAQYLCRIFIPVMDHWEFFGDWGLETWRLGIHDHVKIPNFRWSCELHIREHGHHFKTRFLKHNQFTQCYGLMPDPQFHRSYDVACQWEVTMSQGLMRFHRQNQIEKQRDRTSTYCMMTIGPGFTSNGYDPFVTITITPVQEPVENWFTPGGSMGFMIISRYMQMFFYLTRFSDMTYLVGVHCENYVCWNNVAKFLNGNLQGIFDQGERAYHQFVTWHSYSQYSRCSVGRYACEQAMSRVNSKMTWHWPIRDQGHEHFSEQYLSEKRNPPCNPRIGNAGQHFYEIHRIAHRVAMCNWAPQGQHPGGPTPHDVETCLWLWSLCLKGSDRGYVDRPWMFLADQLGEANLTLITMFHGCTRGCLMWFMDWEECVCSYSVVNPRCHGSEQWSVQNLGWRTCDTLISLWEPECDKHNTPPCLHWEFEDHPSQLRPVMMCDKYVQSIPTDAKWAWTYSKDFVISHWLIWTPIKLEECVFPQINRLWGTACNQGSQKIVIQNVWLRPSSFFQERSKCSDSSCILNVGGSNVNITGKETRTHVPILHMHEIDLISTASSGMRHNLILPHGMLMLHMNWHHSTRAMNPYSSLKLIPWTFQVCETDDRDQNVATHVADPCHKGEDQEIRCCKGGVDHQWKGDRMWMMCMPDMNYVKQDQAPSGTCEGACENYPADKDKCYMIFTIVFDYRRCTKKVCIWISGFPVDAFNLISIANAGFFCCWLEPTELKWRRTFYLGKGTQGWMCTFPHRNIIPVIICAGFGRWVQGEVPFRPVAQISAHSSDRRQGHHPPGTNMCHDYGDQYPIKRVGMQVEEDDGASYCDCAADWKLADMYEADHLSIGVIDFTDWIYPKNGGIWSEIIKSHFHWYHWETPQNTVGAFNTIVGINGSDMCIYHGNTQWEFGWCWKWLNHGHMRNQGPCHLGILEGRISKFAQVTSWWWQTKHDKDWSIEPYGRHWGEAGRPYTYNYCWMRWAIVYNHGNVISVELVPFMDEYPGKCNKEDVQFELFSPMQA"
        val y = "LWFKFLQCIFQYFKDQQETNCIWTFSPFSEHICQRVCQVYWNWNTPSSRTSDPRELFANSTIHNNRCGEWRYMFYHTRTLVQTAPLMKETLHSDGKHSMYCEQRHFFRSSYLIKVNYDVSHYLELYTFSEIPWKLTTHGWDGFSWFLLVNSCCTFDIDGKCGILSQCGMSRAFRTRQEDAYHFQTSLMHLHLHLHVQEGKHEKADLFAQFYNMLPMHGGTCGRNTEPSDLFDSATMNKYMAEHPASCKACPNVSKECFVYWWSHDFTKKHKLIEFSCGRDTGQTTQRTWNVDENEGGKWIWRFHYFMRAKALQIDPKFKPYWNEPRAIMRPGHVTAAPCICAQHSQNETAVCNRDQMHIHAIEFQQYHSRAFGEVQTWCDIGKENENDFIYEQHWWLVGGTEGMAGVIWKFVCARCRTQDCDFWKTCLTYSAQPMMKVYDTIFYVNSINPWEFEDHPSQCDKCVQSIPTDAKYAICGKFVISHWLYWTPQKFEECVHNNVRCAPMGNRLWGTACMVIQNVWLRPSMGSHFSCILNVGGSNINIQGKETWTHVPILHMHEIDLISTASSGMETCKPCFLSGPTIHMGFSYEIRAQPYSRDYFCMDWMQEADEVDHNRCETVQPTLPLLQQFEWKTSCMGQRWITIFCDHCQIVCFSTFFCVMPTFLPNTSILDKFYCIYLSISWTHYCNVHALGFIMRLHYSYMGWKEHKRMHAWDIGLDELWAQEGIQRAQLWCGDEFEVAKYPEWITEARTAIATRPWFHNCYIKPWWIREKHLWFGKESKLDHGHRGAMFTPVANDNTEWMHHWYMFCWAGSKNRLKRQIKEKLIFIIKFMITEFGLFLMIDYTQCYIAWMWAYTGIACYIDWEKCLKHDLTTTDLGCCVYRLFKWYEVRHRAPPQVNTRLPWSQIPMVAIQCNIVDECKEQWHFSYKASFVVEYLCPGCCTNGNRWQWYQVKETPFMYAFAASIFGFHHENLVVFITGSVTIPNGLFGCIAWTSPKPVQKTPASANTIIAYDKCILMG"
        val answer = findMiddleEdge(x, y, readMatrix("/BLOSUM62.txt"), 5)

        assert(answer === List((512, 510), (513, 511)))
    }

    test("Linear Space Alignment") {
        val x = "PLEASANTLY"
        val y = "MEANLY"

        val (score, v, w) = globalAlignment(x, y, readMatrix("/BLOSUM62.txt"), 5)

        assert(score === 8)
        assert(v === "PLEASANTLY")
        assert(w === "-MEA--N-LY")
    }

    def readMatrix(fileName: String): Map[Char, Map[Char, Int]] = {
        val sc = new Scanner(fromURL(getClass.getResource(fileName)).bufferedReader)
        val keys = sc.nextLine.split(" ").filter(_.length > 0)
        sc.all.map { line =>
            val arr = line.split(" ").filter(_.length > 0)
            arr.head.charAt(0) -> arr.tail.zipWithIndex.map { case (value, i) => keys(i).charAt(0) -> value.toInt }.toMap
        }.toMap
    }
}


