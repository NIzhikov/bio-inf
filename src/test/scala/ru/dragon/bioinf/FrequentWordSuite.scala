package ru.dragon.bioinf

import java.util.Scanner

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import scala.io.Source.fromURL

/**
 * @author NIzhikov
 */
@RunWith(classOf[JUnitRunner])
class FrequentWordSuite extends FunSuite with BioInfAlgorithmes {
    test("fw/frequent-words.txt") {
        val sc = new Scanner(fromURL(getClass.getResource("/fw/frequent-words.txt")).bufferedReader)
        val data = sc.nextLine
        assert(data.frequentWords(sc.nextInt).map(_._2).sorted === List("CGGCGGGAGATT", "CGGGAGATTCAA", "CGTGCGGCGGGA", "CGTGGAGGCGTG",
            "CGTGGCGTGCGG", "GCGTGCGGCGGG", "GCGTGGAGGCGT", "GCGTGGCGTGCG", "GGAGAAGCGAGA", "GGAGATTCAAGC",
            "GGCGGGAGATTC", "GGGAGATTCAAG", "GTGCGGCGGGAG", "TGCGGCGGGAGA").sorted)
    }

    test("fw/fw2.txt") {
        val sc = new Scanner(fromURL(getClass.getResource("/fw/fw2.txt")).bufferedReader)
        val data = sc.nextLine
        assert(data.frequentWords(sc.nextInt).map(_._2).sorted === List("GCAT", "CATG").sorted)
    }

    test("fw/dataset_2_9.txt") {
        val sc = new Scanner(fromURL(getClass.getResource("/fw/dataset_2_9.txt")).bufferedReader)
        val data = sc.nextLine
        assert(data.frequentWords(sc.nextInt).map(_._2).sorted === List("AGTTGTTCAGTT", "CAGTTGTTCAGT", "GTTGTTCAGTTG", "TCAGTTGTTCAG", "TTCAGTTGTTCA"))
    }

    test("Thermotoga-petrophila.txt") {
        val sc = new Scanner(fromURL(getClass.getResource("/Thermotoga-petrophila.txt")).bufferedReader)
        val data = sc.nextLine
        assert(data.frequentWords(9, 140).map(_._2).sorted === List("TTCTTTTTC", "TTTTCTTTC", "TTTTTCTTT"))
    }

    test("fw/cf.txt") {
        val sc = new Scanner(fromURL(getClass.getResource("/fw/cf.txt")).bufferedReader)
        val data = sc.nextLine
        assert(data.findClump(sc.nextInt, sc.nextInt, sc.nextInt) === Set("CGACA", "GAAGA"))
    }

    test("fw/dataset_4_4.txt") {
        val sc = new Scanner(fromURL(getClass.getResource("/fw/dataset_4_4.txt")).bufferedReader)
        val data = sc.nextLine
        assert(data.findClump(sc.nextInt, sc.nextInt, sc.nextInt) === Set("TAACGATTAT", "GACCTGGGAC", "CACATCCAGA", "GATACTTTCT", "GCTGGGTGTA", "CCCATTCCCG", "TAGCGCGGCA", "TTTCCGATGG", "ACTCCGCGGA", "AAGGGAACTG"))
    }

    test("E-coli.txt") {
        val sc = new Scanner(fromURL(getClass.getResource("/E-coli.txt")).bufferedReader)
        val data = sc.nextLine
        assert(data.findClump(9, 500, 3).size === 1904)
    }

    test("Frequent Words With Errors - 1") {
        val sc = new Scanner(fromURL(getClass.getResource("/fw/fwe.txt")).bufferedReader)
        val data = sc.nextLine
        assert(data.frequentWordsWithErrors(sc.nextInt, sc.nextInt) === List("GATG", "ATGC", "ATGT"))
    }

    test("Frequent Words With Errors - 2") {
        val sc = new Scanner(fromURL(getClass.getResource("/fw/frequent_words_mismatch_data_1.txt")).bufferedReader)
        val data = sc.nextLine
        assert(data.frequentWordsWithErrors(sc.nextInt, sc.nextInt) === List("GCACACAGAC", "GCGCACACAC"))
    }

    test("fw/dataset_9_7.txt") {
        val sc = new Scanner(fromURL(getClass.getResource("/fw/dataset_9_7.txt")).bufferedReader)
        val data = sc.nextLine
        assert(data.frequentWordsWithErrors(sc.nextInt, sc.nextInt) === List("CGACAGCCA", "CAGCCACCC"))
    }

    test("Frequent Words With Errors And Reverse Complements - 1") {
        val sc = new Scanner(fromURL(getClass.getResource("/fw/fwerc.txt")).bufferedReader)
        val data = sc.nextLine
        assert(data.frequentWordsWithErrorsAndReverse(sc.nextInt, sc.nextInt) === List("ATGT", "ACAT"))
    }

    test("Frequent Words With Errors And Reverse Complements - 2") {
        val sc = new Scanner(fromURL(getClass.getResource("/fw/frequent_words_mismatch_complement.txt")).bufferedReader)
        val data = sc.nextLine
        assert(data.frequentWordsWithErrorsAndReverse(sc.nextInt, sc.nextInt) === List("AGCGCCGCT", "AGCGGCGCT"))
    }

    test("Frequent Words With Errors And Reverse Complements - 3") {
        val sc = new Scanner(fromURL(getClass.getResource("/fw/dataset_9_8.txt")).bufferedReader)
        val data = sc.nextLine
        println(data.frequentWordsWithErrorsAndReverse(sc.nextInt, sc.nextInt).mkString(" "))
    }
}
