package ru.dragon.bioinf

import java.lang.Double.parseDouble
import java.util.Scanner

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import scala.io.Source.fromURL

/**
 * @author NIzhikov
 */
@RunWith(classOf[JUnitRunner])
class MotifSuite extends FunSuite with BioInfAlgorithmes {
    test("Motif Brute Force") {
        assert(List("ATTTGGC", "TGCCTTA", "CGGTATC", "GAAAATT").motifEnumeration(3, 1) === List("ATA", "ATT", "GTT", "TTT"))
    }

    test("Motif Brute Force - ExtraDataset") {
        val sc = new Scanner(fromURL(getClass.getResource("/motif_enumeration_data.txt")).bufferedReader)
        val n = sc.nextInt
        val m = sc.nextInt
        sc.nextLine
        val data = (1 to 6).map(x => sc.nextLine).toList
        val rightAnswer = sc.nextLine.split(" ").toList
        assert(data.motifEnumeration(n, m) === rightAnswer)
    }

    test("Motif Brute Force - 3") {
        val sc = new Scanner(fromURL(getClass.getResource("/dataset_156_7.txt")).bufferedReader)
        val n = sc.nextInt
        val m = sc.nextInt
        sc.nextLine
        val data = (1 to 6).map(x => sc.nextLine).toList
        assert(data.motifEnumeration(n, m).mkString(" ") === "AAAAT AAACG AAATA AACAA AACAG AACCG AACGT AACTA AAGAC AAGAT AAGCA AAGCG AAGGT AAGTA AAGTG AAGTT AATAA AATAC AATAG AATAT AATCA AATCC AATCG AATCT AATGA AATGG AATGT AATTA AATTG ACAAA ACAAT ACACG ACACT ACAGA ACAGC ACATC ACATG ACCAA ACCAG ACCCA ACCCG ACCGA ACCGT ACCTC ACCTG ACGAA ACGAC ACGAT ACGCC ACGCG ACGCT ACGGC ACGGT ACGTA ACGTG ACGTT ACTAA ACTAG ACTAT ACTCA ACTCC ACTCG ACTCT ACTGA ACTGG ACTTA ACTTC ACTTG AGAAC AGAAG AGAAT AGACA AGACC AGACG AGAGA AGAGC AGAGG AGAGT AGATA AGATC AGATG AGATT AGCAA AGCAC AGCAG AGCAT AGCCA AGCCG AGCCT AGCGA AGCGC AGCGG AGCGT AGCTA AGCTC AGCTG AGGAA AGGAC AGGAT AGGCA AGGCC AGGCG AGGCT AGGGA AGGGC AGGTA AGGTC AGGTG AGGTT AGTAA AGTAC AGTAG AGTAT AGTCA AGTCC AGTCG AGTCT AGTGA AGTGC AGTGG AGTTA AGTTC AGTTG ATAAA ATAAC ATAAG ATAAT ATACG ATACT ATAGA ATAGC ATAGG ATAGT ATATT ATCAA ATCAC ATCAG ATCAT ATCCA ATCCG ATCCT ATCGA ATCGC ATCGG ATCGT ATCTC ATCTT ATGAA ATGAC ATGAG ATGAT ATGCG ATGGG ATGGT ATGTA ATGTC ATTAA ATTAC ATTAG ATTAT ATTCA ATTCG ATTCT ATTGA ATTGG ATTGT ATTTA ATTTG CAAAT CAACC CAACT CAAGC CAATA CAATC CAATT CACCC CACCG CACGA CACGT CACTA CACTC CACTG CAGAA CAGAC CAGAG CAGAT CAGCC CAGCG CAGGA CAGGG CAGGT CAGTA CAGTC CAGTG CAGTT CATAA CATAC CATCC CATCG CATCT CATGA CATGC CATGG CATGT CATTA CATTC CCAAG CCAAT CCACC CCACT CCAGA CCAGG CCAGT CCATC CCATT CCCAA CCCAG CCCAT CCCCT CCCGA CCCGC CCCGT CCCTA CCCTT CCGAA CCGAT CCGCA CCGCG CCGCT CCGGC CCGGT CCGTA CCGTC CCGTG CCTAC CCTAG CCTAT CCTCA CCTCG CCTCT CCTGA CCTTC CGAAA CGAAC CGAAG CGAAT CGACA CGACG CGACT CGAGC CGAGG CGAGT CGATA CGATC CGATG CGATT CGCAA CGCGA CGCTA CGCTC CGCTG CGCTT CGGAT CGGCC CGGCT CGGGT CGGTA CGGTC CGGTG CGGTT CGTAA CGTAC CGTCG CGTCT CGTGA CGTGT CGTTC CGTTG CTAAA CTAAC CTAAG CTAAT CTACG CTAGG CTAGT CTATC CTATG CTCAC CTCAT CTCCA CTCCG CTCCT CTCGA CTCGC CTCGG CTCGT CTCTC CTCTG CTGAA CTGAC CTGAG CTGAT CTGGA CTGGC CTGGT CTGTA CTTAC CTTCG CTTGA CTTGC CTTGG GAAAA GAAAG GAAAT GAACA GAACT GAAGA GAAGC GAAGT GAATA GAATC GAATG GAATT GACAA GACAC GACAG GACAT GACCA GACCG GACGA GACGC GACGG GACGT GACTA GACTC GACTT GAGAA GAGAG GAGAT GAGCA GAGCC GAGCG GAGCT GAGGA GAGGC GAGGG GAGGT GAGTA GAGTC GAGTG GAGTT GATAA GATAC GATAG GATAT GATCA GATCG GATCT GATGA GATGG GATGT GATTA GATTC GATTG GATTT GCAAT GCACC GCAGA GCAGG GCAGT GCATA GCATC GCATG GCCAC GCCAG GCCCA GCCCG GCCGA GCCGG GCCTA GCGAA GCGAC GCGAG GCGAT GCGCT GCGGA GCGGT GCGTA GCGTC GCGTT GCTAG GCTCA GCTCC GCTCG GCTCT GCTGA GCTGG GCTTA GCTTG GGAAA GGAAG GGAAT GGACA GGACG GGAGC GGAGG GGAGT GGATA GGATC GGATG GGATT GGCAA GGCAC GGCAT GGCCA GGCGA GGCGG GGCGT GGCTA GGCTC GGCTG GGGAA GGGAG GGGAT GGGCA GGGGT GGGTA GGGTC GGGTT GGTAA GGTAC GGTAG GGTAT GGTCA GGTCG GGTCT GGTGA GGTGC GGTGG GGTTG GTAAA GTAAC GTAAG GTAAT GTACA GTACC GTACT GTAGA GTAGC GTAGG GTAGT GTATC GTATG GTATT GTCAA GTCAC GTCAG GTCCA GTCGA GTCGC GTCGG GTCGT GTCTA GTCTC GTGAA GTGAC GTGAG GTGAT GTGCA GTGCG GTGGA GTGGC GTGGT GTGTC GTGTT GTTAG GTTAT GTTCA GTTCG GTTGA GTTGG GTTTC GTTTG TAAAC TAAAG TAACA TAACG TAACT TAAGA TAAGG TAATA TAATC TAATG TAATT TACAT TACCG TACCT TACGA TACGC TACTC TACTG TACTT TAGAA TAGAG TAGAT TAGCA TAGCG TAGCT TAGGA TAGGC TAGTA TAGTC TAGTG TATAA TATAG TATCC TATCG TATCT TATGC TATGG TATTA TATTC TATTG TCAAG TCAAT TCACC TCACG TCACT TCAGA TCAGG TCATA TCATT TCCAA TCCAC TCCAG TCCAT TCCCA TCCCG TCCCT TCCGA TCCGC TCCGG TCCGT TCCTA TCCTC TCGAA TCGAC TCGAG TCGAT TCGCC TCGCT TCGGA TCGGC TCGGT TCGTC TCGTG TCGTT TCTAA TCTAG TCTCA TCTCC TCTCG TCTGA TCTGC TCTGG TCTGT TCTTA TCTTG TGAAG TGAAT TGACA TGACG TGACT TGAGA TGAGC TGAGG TGAGT TGATA TGATG TGATT TGCAG TGCGA TGCGT TGCTC TGGAA TGGAC TGGAG TGGCA TGGCG TGGGC TGGGG TGGGT TGGTA TGGTG TGTAA TGTAC TGTAG TGTCG TGTCT TTAAG TTAAT TTACA TTACT TTAGC TTAGG TTATA TTATC TTATG TTATT TTCAC TTCAT TTCGA TTCGC TTCGG TTCGT TTCTC TTGAA TTGAG TTGAT TTGGA TTGGC TTGGG TTGGT TTTCG TTTGA")
    }

    test("Motif MedianString") {
        assert(List("AAATTGACGCAT", "GACGACCACGTT", "CGTCAGCGCCTG", "GCTGAGCACCGG", "AGTACGGGACAG").motifMedian(3).contains("GAC"))
    }

    test("Motif MedianString - ExtraDataset") {
        val sc = new Scanner(fromURL(getClass.getResource("/medium_string_data.txt")).bufferedReader)
        val k = sc.nextInt
        sc.nextLine
        val data = (1 to 10).map(x => sc.nextLine).toList
        val rightAnswer = sc.nextLine
        assert(data.motifMedian(k).contains(rightAnswer))
    }

    test("Motif MedianString - 3") {
        val sc = new Scanner(fromURL(getClass.getResource("/dataset_158_9.txt")).bufferedReader)
        val k = sc.nextInt
        sc.nextLine
        val data = (1 to 10).map(x => sc.nextLine).toList
        assert(data.motifMedian(k).mkString(" ") === "ATACCA")
    }

    test ("Greedy Motif Search") {
        val str = "ACCTGTTTATTGCCTAAGTTCCGAACAAACCCAATATAGCCCGAGGGCCT"
        val k = 5
        val profile = Map[Char, List[Double]](
            'A' -> List(0.2, 0.2, 0.3, 0.2, 0.3),
            'C' -> List(0.4, 0.3, 0.1, 0.5, 0.1),
            'G' -> List(0.3, 0.3, 0.5, 0.2, 0.4),
            'T' -> List(0.1, 0.2, 0.1, 0.1, 0.2))

        assert(str.greedyMotifSearch(k, profile) === "CCGAG")
    }

    test ("Greedy Motif Search - ExtraDataset") {
        val sc = new Scanner(fromURL(getClass.getResource("/profile_most_1.txt")).bufferedReader)
        val str = sc.nextLine
        val k = sc.nextInt

        sc.nextLine
        val profile = List('A', 'C', 'G', 'T').map(ch => { ch -> sc.nextLine.split(" ").map(parseDouble).toList }).toMap
        val answer = sc.nextLine

        assert(str.greedyMotifSearch(k, profile) === answer)
    }

    test ("Greedy Motif Search - 3") {
        val sc = new Scanner(fromURL(getClass.getResource("/dataset_159_3.txt")).bufferedReader)
        val str = sc.nextLine
        val k = sc.nextInt

        sc.nextLine
        val profile = List('A', 'C', 'G', 'T').map(ch => { ch -> sc.nextLine.split(" ").map(parseDouble).toList }).toMap

        assert(str.greedyMotifSearch(k, profile) === "ACCGTGGCTTTGCAT")
    }

    test("Motif Greedy Search - 2") {
        val answer = List("GGCGTTCAGGCA", "AAGAATCAGTCA", "CAAGGAGTTCGC", "CACGTCAATCAC", "CAATAATATTCG").
            greedyMotifSearch(3, 5)
        assert(answer === List("CAG", "CAG", "CAA", "CAA", "CAA"))
    }

    test("Motif Greedy Search - 2 - ExtraDataset") {
        val sc = new Scanner(fromURL(getClass.getResource("/greedy_data.txt")).bufferedReader)
        val k = sc.nextInt
        val t = sc.nextInt
        sc.nextLine

        val data = (1 to 25).map(x => sc.nextLine).toList
        val rightAnswer = (1 to 25).map(x => sc.nextLine).toList
        assert(data.greedyMotifSearch(k, t) === rightAnswer)
    }
}
